#ifndef __SPIRALMAP_H__
#define __SPIRALMAP_H__

#include <utility>
#include "ChainsTable.h"
#include "SpiralBase.h"

namespace algo
{

template <typename HashKeyT, typename K, typename V, int BucketLength = 32>
struct MapBucket
{
    typedef HashKeyT hash_key_type;
    typedef K key_type;
    typedef std::pair<K, V> item_type;

    static const int cnBucketLength = BucketLength;

    ChainData<HashKeyT, K, BucketLength> chain;
    item_type items[BucketLength];    
};

template <typename EnumeratorT>
class CSpiralMapConstIterator;

template <typename MapT, typename KeyT, typename ValueT>
class CSpiralMapInserter;

template <typename HashFunT, typename StorageT>
class CSpiralMapImpl : public CSpiralBase<HashFunT, StorageT>
{
    typedef CSpiralMapImpl<HashFunT, StorageT> this_type;
    typedef CSpiralBase<HashFunT, StorageT> spiral_base;        
public:    
    typedef typename item_type::second_type value_type;
    typedef CSpiralMapConstIterator<items_enumerator_type> const_iterator;
    typedef CConstPosition<item_type, const_iterator> const_position;
    typedef CSpiralMapInserter<this_type, key_type, typename item_type::second_type> inserter;

    CSpiralMapImpl(StorageT* pStorage);

    std::pair<const_position, bool> insert(const item_type& item);
    std::pair<const_position, bool> update(const item_type& item);
    const_position find(const key_type& key) const;
    size_t erase(const key_type& key);
    void clear();
    void swap(this_type& other);

    size_t size() const;

    inserter operator [] (const key_type& key);

    const_iterator begin() const;
    const_iterator end() const;
};

template <typename HashFunT, typename StorageT>
CSpiralMapImpl<HashFunT, StorageT>::CSpiralMapImpl(StorageT* pStorage)
    : spiral_base(pStorage)
{
}

template <typename HashFunT, typename StorageT>
std::pair<typename CSpiralMapImpl<HashFunT, StorageT>::const_position, bool> 
    CSpiralMapImpl<HashFunT, StorageT>::insert(const item_type& item)
{        
    spiral_base::split();
    
    const hash_key_type hashKey = HashFunT().process(item.first);
    chain_type* pChain = 0;

    //if key already added to chunk, then exit
    if (0 != (pChain = spiral_base::findItem(hashKey, item.first)))
    {
        return std::pair<const_position, bool>(const_position(pChain->currItem()), false);
    }

    //otherwise insert new item
    pChain = spiral_base::insertItem(hashKey, item);
    return std::pair<const_position, bool>(const_position(pChain->currItem()), true);    
}

template <typename HashFunT, typename StorageT>
std::pair<typename CSpiralMapImpl<HashFunT, StorageT>::const_position, bool> 
    CSpiralMapImpl<HashFunT, StorageT>::update(const item_type& item)
{
    spiral_base::split();

    const hash_key_type hashKey = HashFunT().process(item.first);
    chain_type* pChain = 0;

    //if key already added to chunk, then update item    
    if (0 != (pChain = spiral_base::findItem(hashKey, item.first)))
    {
        pChain->updateItem(item.second);
        return std::pair<const_position, bool>(const_position(pChain->currItem()), false);
    }

    //otherwise insert new item
    pChain = spiral_base::insertItem(hashKey, item);
    return std::pair<const_position, bool>(const_position(pChain->currItem()), true);    
}

template <typename HashFunT, typename StorageT>
typename CSpiralMapImpl<HashFunT, StorageT>::const_position 
    CSpiralMapImpl<HashFunT, StorageT>::find(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);    
    if (0 != pChain)
    {        
        return const_position(pChain->currItem());
    }
    return const_position();    
}

template <typename HashFunT, typename StorageT>
size_t CSpiralMapImpl<HashFunT, StorageT>::erase(const key_type& key)
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);
    if (0 != pChain)
    {
        spiral_base::eraseCurrentItem();
        spiral_base::group();

        return 1;
    }
    return 0;
}

template <typename HashFunT, typename StorageT>
void CSpiralMapImpl<HashFunT, StorageT>::clear()
{
    spiral_base::clear();
}

template <typename HashFunT, typename StorageT>
void CSpiralMapImpl<HashFunT, StorageT>::swap(this_type& other)
{
    spiral_base::swap(other);
}

template <typename HashFunT, typename StorageT>
size_t CSpiralMapImpl<HashFunT, StorageT>::size() const
{
    return spiral_base::getItemsCount();
}

template <typename HashFunT, typename StorageT>
typename CSpiralMapImpl<HashFunT, StorageT>::inserter 
    CSpiralMapImpl<HashFunT, StorageT>::operator [] (const key_type& key)
{
    return inserter(this, key);
}

template <typename HashFunT, typename StorageT>
typename CSpiralMapImpl<HashFunT, StorageT>::const_iterator 
    CSpiralMapImpl<HashFunT, StorageT>::begin() const
{
    return const_iterator(getItemsEnumerator());
}

template <typename HashFunT, typename StorageT>
typename CSpiralMapImpl<HashFunT, StorageT>::const_iterator 
    CSpiralMapImpl<HashFunT, StorageT>::end() const
{
    return const_iterator();
}

template <typename MapT, typename KeyT, typename ValueT>
class CSpiralMapInserter
{
public:
    CSpiralMapInserter(MapT* pMap, const KeyT& key)
        : m_pMap(pMap), m_key(key)
    {
    }

    CSpiralMapInserter& operator = (const ValueT& value)
    {
        m_pMap->update(std::pair<KeyT, ValueT>(m_key, value));                
        return *this;
    }

    operator const ValueT& ()
    {
        MapT::const_position itemPos = m_pMap->find(m_key);
        if (itemPos == m_pMap->end())
        {
            std::pair<MapT::const_position, bool> pairIns = 
                m_pMap->insert(std::pair<KeyT, ValueT>(m_key, ValueT()));

            return pairIns.first->second;
        }
        return itemPos->second;
    }

private:
    MapT* m_pMap;
    KeyT m_key;
};

template <typename EnumeratorT>
class CSpiralMapConstIterator
{    
    typedef EnumeratorT enumerator_type;
    typedef typename enumerator_type::item_type item_type;
    typedef typename enumerator_type::key_type key_type;

    friend CConstPosition<item_type, CSpiralMapConstIterator<enumerator_type> >;

public:    
    typedef CSpiralMapConstIterator<enumerator_type> const_iterator;
    typedef const item_type* const_pointer;
    typedef const item_type& const_reference;

    CSpiralMapConstIterator()
        : m_pItem(0)
    {
    }

    CSpiralMapConstIterator(const enumerator_type& enumerator)
        : m_pItem(0), m_enumerator(enumerator)
    {         
        m_pItem = m_enumerator.currItem();
    }

    CSpiralMapConstIterator(const CSpiralMapConstIterator& other)
        : m_enumerator(other.m_enumerator), m_pItem(0)
    {
        m_pItem = m_enumerator.currItem();
    }

    CSpiralMapConstIterator& operator = (const CSpiralMapConstIterator& other)
    {        
        m_enumerator = other.m_enumerator;
        m_pItem = m_enumerator.currItem();
        return *this;
    }

    const_reference operator*() const
    {
        return *m_pItem;
    }

    const_pointer operator->() const
    {
        return m_pItem;
    }

    const_iterator& operator++()
    {
        m_pItem = m_enumerator.nextItem();
        return *this;
    }

    const_iterator operator++(int)
    {        
        CSpiralMapConstIterator prevIter = *this; 
        m_pItem = m_enumerator.nextItem();
        return prevIter;
    }

    bool operator == (const const_iterator& other)
    {
        return (m_pItem == other.m_pItem);
    }

    bool operator != (const const_iterator& other)
    {        
        return !(*this == other);
    }

private:
    const item_type* m_pItem;
    enumerator_type m_enumerator;
};

} //namespace algo

#endif //__SPIRALMAP_H__