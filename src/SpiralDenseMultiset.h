#ifndef __SPIRALDENSEMULTISET_H__
#define __SPIRALDENSEMULTISET_H__

#include <utility>
#include "ChainsTable.h"
#include "SpiralBase.h"

namespace algo
{

template <typename HashKeyT, typename K, int BucketLength = 32>
struct DenseMultisetBucket
{
    typedef HashKeyT hash_key_type;
    typedef K key_type;
    typedef std::pair<K, int> item_type;

    static const int cnBucketLength = BucketLength;

    ChainData<HashKeyT, K, BucketLength> chain;
    item_type items[BucketLength];    
};

template <typename EnumeratorT>
class CSpiralDenseMultisetConstIterator;

template <typename ItemT, typename KeyT>
class CConstMultisetPosition;

template <typename HashFunT, typename StorageT>
class CSpiralDenseMultisetImpl : public CSpiralBase<HashFunT, StorageT>
{
    typedef CSpiralDenseMultisetImpl<HashFunT, StorageT> this_type;
    typedef CSpiralBase<HashFunT, StorageT> spiral_base;
public:
    typedef CSpiralDenseMultisetConstIterator<items_enumerator_type> const_iterator;

    CSpiralDenseMultisetImpl(StorageT* pStorage);    

    void insert(const key_type& key);
    const_iterator find(const key_type& key) const;
    bool contains(const key_type& key) const;
    size_t erase(const key_type& key);
    void clear();
    void swap(this_type& other);

    size_t size() const;
    size_t unique() const;

    const_iterator begin() const;
    const_iterator end() const;

private:
    size_t m_nElements;
};

template <typename HashFunT, typename StorageT>
CSpiralDenseMultisetImpl<HashFunT, StorageT>::CSpiralDenseMultisetImpl(StorageT* pStorage)
    : spiral_base(pStorage), m_nElements(0)
{
}

template <typename HashFunT, typename StorageT>
void CSpiralDenseMultisetImpl<HashFunT, StorageT>::insert(const key_type& key)
{
    spiral_base::split();

    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = 0;

    //if key already added to chunk, then increment counter    
    if (0 != (pChain = spiral_base::findItem(hashKey, key)))
    {        
        //increase elements counter
        const item_type* pItem = pChain->currItem();        
        pChain->updateItem(pItem->second + 1);                
    }
    else
    {
        //otherwise insert new item
        const item_type itemNew(key, 1); //initialize counter to one element
        pChain = spiral_base::insertItem(hashKey, itemNew);        
    }

    ++m_nElements;
}

template <typename HashFunT, typename StorageT>
typename CSpiralDenseMultisetImpl<HashFunT, StorageT>::const_iterator 
    CSpiralDenseMultisetImpl<HashFunT, StorageT>::find(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);    
    if (0 != pChain)
    {        
        return const_iterator(pChain->currItem());
    }
    return end();    
}

template <typename HashFunT, typename StorageT>
bool CSpiralDenseMultisetImpl<HashFunT, StorageT>::contains(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    return (0 != spiral_base::findItem(hashKey, key));
}

template <typename HashFunT, typename StorageT>
size_t CSpiralDenseMultisetImpl<HashFunT, StorageT>::erase(const key_type& key)
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);
    if (0 != pChain)
    {
        size_t nRepeats = pChain->currItem()->second;

        spiral_base::eraseCurrentItem();
        spiral_base::group();

        m_nElements -= nRepeats;
        return nRepeats;
    }
    return 0;
}

template <typename HashFunT, typename StorageT>
void CSpiralDenseMultisetImpl<HashFunT, StorageT>::clear()
{
    spiral_base::clear();
}

template <typename HashFunT, typename StorageT>
void CSpiralDenseMultisetImpl<HashFunT, StorageT>::swap(this_type& other)
{
    std::swap(m_nElements, other.m_nElements);
    spiral_base::swap(other);
}

template <typename HashFunT, typename StorageT>
size_t CSpiralDenseMultisetImpl<HashFunT, StorageT>::size() const
{
    return m_nElements;
}

template <typename HashFunT, typename StorageT>
size_t CSpiralDenseMultisetImpl<HashFunT, StorageT>::unique() const
{
    return spiral_base::getItemsCount();
}

template <typename HashFunT, typename StorageT>
typename CSpiralDenseMultisetImpl<HashFunT, StorageT>::const_iterator 
    CSpiralDenseMultisetImpl<HashFunT, StorageT>::begin() const
{
    return const_iterator(getItemsEnumerator());
}

template <typename HashFunT, typename StorageT>
typename CSpiralDenseMultisetImpl<HashFunT, StorageT>::const_iterator 
    CSpiralDenseMultisetImpl<HashFunT, StorageT>::end() const
{
    return const_iterator();
}

template <typename EnumeratorT>
class CSpiralDenseMultisetConstIterator
{    
    typedef EnumeratorT enumerator_type;
    typedef typename enumerator_type::item_type item_type;
    typedef typename enumerator_type::key_type key_type;

public:    
    typedef CSpiralDenseMultisetConstIterator<enumerator_type> const_iterator;
    typedef const key_type* const_pointer;
    typedef const key_type& const_reference;

    CSpiralDenseMultisetConstIterator()
        : m_pItem(0), m_nRepeats(0)
    {
    }

    CSpiralDenseMultisetConstIterator(const enumerator_type& enumerator)
        : m_pItem(0), m_nRepeats(0), m_enumerator(enumerator)
    {         
        m_pItem = m_enumerator.currItem();
        if (0 != m_pItem)
        {
            m_nRepeats = m_pItem->second;
        }
    }

    CSpiralDenseMultisetConstIterator(const item_type* pItem)
        : m_pItem(pItem), m_nRepeats(pItem->second)
    {
    }

    CSpiralDenseMultisetConstIterator(const CSpiralDenseMultisetConstIterator& other)
        : m_pItem(0), m_nRepeats(other.m_nRepeats), m_enumerator(other.m_enumerator)
    {
        m_pItem = m_enumerator.currItem();   
    }

    CSpiralDenseMultisetConstIterator& operator = (const CSpiralDenseMultisetConstIterator& other)
    {        
        m_enumerator = other.m_enumerator;
        m_pItem = m_enumerator.currItem();
        m_nRepeats = other.m_nRepeats;
        return *this;
    }

    const_reference operator*() const
    {
        return m_pItem->first;
    }

    const_pointer operator->() const
    {
        return &m_pItem->first;
    }

    const_iterator& operator++()
    {
        incrementPosition();
        return *this;
    }

    const_iterator operator++(int)
    {        
        CSpiralDenseMultisetConstIterator prevIter = *this;         
        incrementPosition();
        return prevIter;
    }

    bool operator == (const const_iterator& other)
    {
        return (m_pItem == other.m_pItem);
    }

    bool operator != (const const_iterator& other)
    {
        return !(*this == other);
    }

    bool isLastKeyInGroup() const
    {
        return 1 == m_nRepeats;
    }

private:
    const item_type* m_pItem;
    int m_nRepeats;
    
    enumerator_type m_enumerator;

    void incrementPosition()
    {
        if (--m_nRepeats == 0)
        {
            if (m_enumerator.empty())
            {
               m_pItem = 0;
               return;
            }

            m_pItem = m_enumerator.nextItem();
            if (0 != m_pItem)
            {
                m_nRepeats = m_pItem->second;
            }            
        }
    }
};

} //namespace algo

#endif //__SPIRALDENSEMULTISET_H__