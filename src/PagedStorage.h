#ifndef __PAGEDSTORAGE_H__
#define __PAGEDSTORAGE_H__

#include "../pmio/PMIOManager.h"
#include "../common/QDExceptions.h"
#include "../common/qd_errors.h"

namespace algo
{

template <typename BucketT>
class CPagedStorage
{    
public:
    typedef typename BucketT::item_type item_type;
    typedef typename BucketT bucket_type;     

    class CBucketHolder
    {    
    public:
        CBucketHolder() : m_pPage(0), m_pBucket(0) {}
        CBucketHolder(pmio::CPMIOPage* pPage, BucketT* pBucket) : m_pPage(pPage), m_pBucket(pBucket) {}
        CBucketHolder(const CBucketHolder& other) : m_pPage(0), m_pBucket(0)
        { 
            *this = other;
        }
        CBucketHolder& operator = (const CBucketHolder& other)
        {
            if (m_pBucket != other.m_pBucket)
            {
                if (0 != m_pPage)            
                    m_pPage->unlock(false);

                m_pPage = other.m_pPage;
                m_pBucket = other.m_pBucket;        
                other.m_pPage = 0;
            }
            return *this;
        }
        ~CBucketHolder()
        {
            if (0 != m_pPage)
                m_pPage->unlock(false);
        }
        BucketT* get() { return m_pBucket; }
        const BucketT* get() const { return m_pBucket; } 
        BucketT* operator -> () { return get(); }
        const BucketT* operator -> () const { return get(); }
        void swap(CBucketHolder& other) { std::swap(m_pBucket, other.m_pBucket); }
    private:        
        mutable pmio::CPMIOPage* m_pPage;
        BucketT* m_pBucket;        
    };

    CPagedStorage();

    CBucketHolder readBucket(int nPhysBuckIndex);
    void writeBucket(int nPhysBuckIndex, const CBucketHolder&);

    void expand();
    void contract();
    void clear();

    void shrink() {}

    bool isNeedContract() const { return m_vecPages.size() > 1; }
    size_t getUsedPagesCount() const { return m_vecPages.size(); }

    void swap(CPagedStorage& other);

private:
    template <bool> class CSizeAssert;
    template<> class CSizeAssert<true> {};
    CSizeAssert< sizeof(BucketT) <= pmio::PMIO_PAGE_SIZE > checkSz;

    template<typename T>
    struct CPageUnlockT
    {
        static void     construct(T v) { v->lock(); }
        static void     destroy(T v) { v->unlock(false); }
    };
    typedef memory::CAutoT<pmio::CPMIOPage*, CPageUnlockT<pmio::CPMIOPage*> > CAutoPage;

    typedef std::vector<pmio::CPMIOPage*> pages_vector_type;
    static const size_t m_cnBucketsInPage;

    size_t getPageIndex(int nPhysBuckIndex) const
    {
        return nPhysBuckIndex / m_cnBucketsInPage;
    }

    size_t getBucketInPage(int nPhysBuckIndex) const
    {
        return nPhysBuckIndex % m_cnBucketsInPage;
    }

    pages_vector_type m_vecPages;
    size_t m_nBucketsCount;
};

template <typename BucketT>
const size_t CPagedStorage<BucketT>::m_cnBucketsInPage 
    = pmio::PMIO_PAGE_SIZE / sizeof(BucketT);

template <typename BucketT>
CPagedStorage<BucketT>::CPagedStorage()
    : m_nBucketsCount(0)
{    
}

template <typename BucketT>
typename CPagedStorage<BucketT>::CBucketHolder CPagedStorage<BucketT>::readBucket(int nPhysBuckIndex)
{
    if (0 == m_nBucketsCount)
    {
        expand();
    }

    assert(getPageIndex(nPhysBuckIndex) < m_vecPages.size());
    pmio::CPMIOPage *pPage = m_vecPages[getPageIndex(nPhysBuckIndex)];
    if (!pPage->lock())
    {
        throw qderror(-1, "Page lock error");
    }
    
    size_t nBuckOffset = getBucketInPage(nPhysBuckIndex) * sizeof(BucketT);
    BucketT* pBucket = reinterpret_cast<BucketT*>(pPage->ptr() + nBuckOffset);

    return CBucketHolder(pPage, pBucket);
}

template <typename BucketT> 
void CPagedStorage<BucketT>::writeBucket(int nPhysBuckIndex, const CBucketHolder&)
{
    if (0 == m_nBucketsCount)
    {
        expand();
    }

    assert(getPageIndex(nPhysBuckIndex) < m_vecPages.size());
    pmio::CPMIOPage *pPage = m_vecPages[getPageIndex(nPhysBuckIndex)];
    pPage->setDirty();
}

template <typename BucketT>
void CPagedStorage<BucketT>::expand()
{        
    ++m_nBucketsCount;
    
    if (m_nBucketsCount > m_vecPages.size() * m_cnBucketsInPage)
    {                    
        pmio::CPMIOPage* pNewPage = pmio::CPMIOManager::getFreePage();
        pNewPage->unlock(false);
        m_vecPages.push_back(pNewPage);
    }
    
    const int nLastBucket = static_cast<int>(m_nBucketsCount - 1);
    pmio::CPMIOPage* pPage = m_vecPages[getPageIndex(nLastBucket)];
    size_t nBuckOffset = getBucketInPage(nLastBucket) * sizeof(BucketT);
    
    CAutoPage pageInit(pPage);    
    new (pageInit->ptr() + nBuckOffset) BucketT();    
}

template <typename BucketT>
void CPagedStorage<BucketT>::contract()
{
    --m_nBucketsCount;

    const int nLastBucket = static_cast<int>(m_nBucketsCount);
    pmio::CPMIOPage* pPage = m_vecPages[getPageIndex(nLastBucket)];
    size_t nBuckOffset = getBucketInPage(nLastBucket) * sizeof(BucketT);
    
    if (!pPage->lock())
    {
        throw qderror(-1, "Page lock error");
    }
    BucketT* pBucket = reinterpret_cast<BucketT*>(pPage->ptr() + nBuckOffset);
    pBucket->~BucketT();
    pPage->unlock(false);

    if ((m_nBucketsCount / m_cnBucketsInPage) + 1 < m_vecPages.size())
    {
        pmio::CPMIOPage* pPage = m_vecPages.back();
        assert(!pPage->isLocked());
        pPage->release();
        m_vecPages.resize(m_vecPages.size() - 1);
    }        
}

template <typename BucketT>
void CPagedStorage<BucketT>::clear()
{    
    pages_vector_type::const_iterator iterPage = m_vecPages.begin();
    for ( ; iterPage != m_vecPages.end(); ++iterPage)
    {
        pmio::CPMIOPage* pPage = *iterPage;
        assert(!pPage->isLocked());
        pPage->release();
    }
    m_vecPages.swap(pages_vector_type());
    m_nBucketsCount = 0;
}

template <typename BucketT>
void CPagedStorage<BucketT>::swap(CPagedStorage& other)
{
    m_vecPages.swap(other.m_vecPages);
    std::swap(m_nBucketsCount, other.m_nBucketsCount);
}

} //namespace algo

#endif //__PAGEDSTORAGE_H__