#ifndef __DEFHASHFUNC_H__
#define __DEFHASHFUNC_H__

namespace algo
{

template <typename T>
class CDefHashFunc
{
public:
    typedef unsigned int value_type;

    static unsigned int process(const T& data)
    {
        //rot13 hash algorithm
        const unsigned char* str = reinterpret_cast<const unsigned char*>(&data);
        unsigned int hash = 0;
        unsigned int i = 0;

        for (i = 0; i < sizeof(T); str++, i++) {
            hash += (unsigned char)(*str);
            hash -= (hash << 13) | (hash >> 19);
        }
        return hash;
    }
};

template <typename T>
class CBaseDblWordTypeHashFunc
{
public:
    typedef unsigned int value_type;

    static unsigned int process(const T& data)
    {        
        //reverse bits in double word
        unsigned int nValue = static_cast<unsigned int>(data);
        nValue = (nValue & 0x55555555) <<  1 | (nValue & 0xAAAAAAAA) >>  1;
        nValue = (nValue & 0x33333333) <<  2 | (nValue & 0xCCCCCCCC) >>  2;
        nValue = (nValue & 0x0F0F0F0F) <<  4 | (nValue & 0xF0F0F0F0) >>  4;
        nValue = (nValue & 0x00FF00FF) <<  8 | (nValue & 0xFF00FF00) >>  8;
        nValue = (nValue & 0x0000FFFF) << 16 | (nValue & 0xFFFF0000) >> 16;
        return nValue;
    }
};

template <typename T>
class CBaseWordTypeHashFunc
{
public:
    typedef unsigned short value_type;

    static unsigned short process(const T& data)
    {
        //reverse bits in word
        unsigned short wValue = static_cast<unsigned short>(data);
        wValue = (wValue & 0x5555) <<  1 | (wValue & 0xAAAA) >>  1;
        wValue = (wValue & 0x3333) <<  2 | (wValue & 0xCCCC) >>  2;
        wValue = (wValue & 0x0F0F) <<  4 | (wValue & 0xF0F0) >>  4;
        wValue = (wValue & 0x00FF) <<  8 | (wValue & 0xFF00) >>  8;        
        return wValue;
    }
};

template <typename T>
class CBaseByteTypeHashFunc
{
public:
    typedef unsigned char value_type;

    static unsigned char process(const T& data)
    {
        //reverse bits in byte
        unsigned char bValue = static_cast<unsigned char>(data);
        bValue = (bValue & 0x55) <<  1 | (bValue & 0xAA) >>  1;
        bValue = (bValue & 0x33) <<  2 | (bValue & 0xCC) >>  2;
        bValue = (bValue & 0x0F) <<  4 | (bValue & 0xF0) >>  4;        
        return bValue;
    }
};

//specialization for basic types
template <> class CDefHashFunc<int> : public CBaseDblWordTypeHashFunc<int> {};
template <> class CDefHashFunc<unsigned int> : public CBaseDblWordTypeHashFunc<unsigned int> {};
template <> class CDefHashFunc<long> : public CBaseDblWordTypeHashFunc<long> {};
template <> class CDefHashFunc<unsigned long> : public CBaseDblWordTypeHashFunc<unsigned long> {};
template <> class CDefHashFunc<short> : public CBaseWordTypeHashFunc<short> {};
template <> class CDefHashFunc<unsigned short> : public CBaseWordTypeHashFunc<unsigned short> {};
template <> class CDefHashFunc<char> : public CBaseByteTypeHashFunc<char> {};
template <> class CDefHashFunc<unsigned char> : public CBaseByteTypeHashFunc<unsigned char> {};

}

#endif //__DEFHASHFUNC_H__