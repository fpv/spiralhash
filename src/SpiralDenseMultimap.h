#ifndef __SPIRALDENSEMULTIMAP_H__
#define __SPIRALDENSEMULTIMAP_H__

#include <utility>
#include "ChainsTable.h"
#include "SpiralBase.h"

namespace algo
{

template <typename HashKeyT, typename K, typename V, int BucketLength = 32>
struct DenseMultimapBucket
{    
    typedef HashKeyT hash_key_type;
    typedef K key_type;
    typedef std::pair<int, V> accessor_type;
    typedef std::pair<K, accessor_type> item_type;

    static const int cnBucketLength = BucketLength;

    ChainData<HashKeyT, K, BucketLength> chain;
    item_type items[BucketLength];
};


template <typename V, int BucketLength = 16>
struct ValuesMultimapBucket
{    
    struct single 
    {         
        V first; 

        single() {}
        single(const V& f) : first(f) {}
    };

    typedef int hash_key_type;
    typedef V key_type;
    typedef single item_type;

    static const int cnBucketLength = BucketLength;

    ChainData<int, ValueStub, BucketLength> chain;
    item_type items[BucketLength];    

    int nNextFreeBucket;

    ValuesMultimapBucket() : nNextFreeBucket(0) {}
};

struct ValueStubFunc
{
    typedef int value_type;
    static int process(const value_type&) { return 0; }
};

template <typename EnumeratorT, typename ValuesTableT>
class CSpiralDenseMultimapConstIterator;

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
class CSpiralDenseMultimapImpl : public CSpiralBase<HashFunT, KeyStorageT>
{        
    typedef CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT> this_type;
    typedef CSpiralBase<HashFunT, KeyStorageT> spiral_base;
    typedef CChainsTable<ValueStorageT, 1> val_table_type;
    typedef typename bucket_type::accessor_type accessor_type;
public:    
    typedef ValueStorageT value_storage_type;
    typedef typename accessor_type::second_type value_type;
    typedef std::pair<key_type, value_type> value_item_type;
    typedef CSpiralDenseMultimapConstIterator<items_enumerator_type, val_table_type> const_iterator;
    
    CSpiralDenseMultimapImpl(KeyStorageT* pKeyStorage, ValueStorageT* pValueStorage);
    ~CSpiralDenseMultimapImpl();

    void insert(const value_item_type& item);
    const_iterator find(const key_type& key) const;
    bool contains(const key_type& key) const;
    size_t erase(const key_type& key);
    void clear();
    void swap(this_type& other);

    size_t size() const;
    size_t unique() const;

    double getValuesLoadFactor() const;

    const_iterator begin() const;
    const_iterator end() const;

protected:
    void reset(KeyStorageT* pKeyStorage, ValueStorageT* pValueStorage);

private:        
    typedef typename val_table_type::item_type val_item_type;

    static int valueBuckIndexToPhysIndex(int nLogicalIndex);
    int getFreeBucketIndex();
    void expandValueStorage();
    void groupValueTable();

    val_table_type m_tableValues;
    int m_nNextFreeBucket;    
};

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::
    CSpiralDenseMultimapImpl(KeyStorageT* pKeyStorage, ValueStorageT* pValueStorage)
    : spiral_base(pKeyStorage), 
    m_tableValues(pValueStorage, &this_type::valueBuckIndexToPhysIndex), 
    m_nNextFreeBucket(1)
{
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::~CSpiralDenseMultimapImpl()
{
    clear();
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
void CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::reset(KeyStorageT* pKeyStorage, ValueStorageT* pValueStorage)
{
    spiral_base::reset(pKeyStorage);
    m_tableValues.setStorage(pValueStorage);
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
void CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::insert(const value_item_type& item)
{        
    spiral_base::split();

    const hash_key_type hashKey = HashFunT().process(item.first);
    chain_type* pChain = 0;

    if (0 != (pChain = spiral_base::findItem(hashKey, item.first)))
    {
        //item already exists
        const item_type* pItem = pChain->currItem();
        const accessor_type& acsr = pItem->second;
        
        if (0 == acsr.first) //have only one value for this key
        {
            int nFreeBucketIndex = getFreeBucketIndex();
            
            val_table_type::CChain* pValChain = m_tableValues.seekToChain(nFreeBucketIndex);
            pValChain->seekLastItem();

            typename val_table_type::bucket_type* pPrimValBucket = pValChain->getPrimaryBucket();
            m_nNextFreeBucket = pPrimValBucket->nNextFreeBucket;

            const val_item_type valItem(item.second);            
            
            if (-1 == pValChain->insertItem(0, valItem, false))
            {
                expandValueStorage();
                nFreeBucketIndex = getFreeBucketIndex();

                pValChain = m_tableValues.seekToChain(nFreeBucketIndex);
                pValChain->seekLastItem();
                pValChain->insertItem(0, valItem, false);
            }

            const accessor_type acsrUpd(nFreeBucketIndex, acsr.second);
            pChain->updateItem(acsrUpd);
        }
        else //have more then one chained values
        {
            val_table_type::CChain* pValChain = m_tableValues.seekToChain(acsr.first);
            pValChain->seekLastItem();

            const val_item_type valItem(item.second);            
            
            if (-1 == pValChain->insertItem(0, valItem, false))
            {
                expandValueStorage();
                
                pValChain = m_tableValues.seekToChain(acsr.first);
                pValChain->seekLastItem();
                pValChain->insertItem(0, valItem, false);
            }
        }        
    }
    else
    {
        //otherwise only insert new item
        const item_type itemNew(item.first, accessor_type(0, item.second));
        pChain = spiral_base::insertItem(hashKey, itemNew);
    }    
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
typename CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::const_iterator 
    CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::find(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);
    if (0 != pChain)
    {        
        return const_iterator(pChain->currItem(), m_tableValues);
    }
    return end();    
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
bool CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::contains(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    return (0 != spiral_base::findItem(hashKey, key));
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
size_t CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::erase(const key_type& key)
{
    const hash_key_type hashKey = HashFunT().process(key);
    
    chain_type* pChain = spiral_base::findItem(hashKey, key);
    if (0 != pChain)
    {
        size_t nValuesErased = 1;
        const item_type* pItem = pChain->currItem();
        const accessor_type& acsr = pItem->second;

        //if item has more then one value
        //then erase all values from values table
        if (acsr.first != 0)
        {
            val_table_type::CChain* pValChain = m_tableValues.seekToChain(acsr.first);
            pValChain->getPrimaryBucket()->nNextFreeBucket = m_nNextFreeBucket;
            
            int stubHashKey = 0;
            const val_item_type* pChainItem = 0;
            while (pChainItem = pValChain->detachNextItem(ValueStubFunc(), stubHashKey))
            {
                ++nValuesErased;
            }            
            
            m_nNextFreeBucket = acsr.first;

            groupValueTable();
        }

        spiral_base::eraseCurrentItem();
        spiral_base::group();

        return nValuesErased;
    }
    return 0;
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
void CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::clear()
{
    spiral_base::clear();
    m_tableValues.clear();
    m_nNextFreeBucket = 0;
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
void CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::swap(this_type& other)
{
    m_tableValues.swap(other.m_tableValues);
    std::swap(m_nNextFreeBucket, other.m_nNextFreeBucket);
    spiral_base::swap(other);
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
int CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::valueBuckIndexToPhysIndex(int nLogicalIndex)
{
    return nLogicalIndex - 1;
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
int CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::getFreeBucketIndex()
{
    if (0 == m_nNextFreeBucket || m_nNextFreeBucket > m_tableValues.getLastIndex())
    {
        m_nNextFreeBucket = 0;
        expandValueStorage();
    }

    return m_nNextFreeBucket;
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
void CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::expandValueStorage()
{
    m_tableValues.getStorage()->expand();
    m_tableValues.setLastIndex(m_tableValues.getLastIndex() + 1);
    const int nNewFreeBucket = m_tableValues.getLastIndex();

    val_table_type::CChain* pValChain = m_tableValues.seekToChain(nNewFreeBucket);
    typename val_table_type::bucket_type* pPrimValBucket = pValChain->getPrimaryBucket();
    pPrimValBucket->nNextFreeBucket = m_nNextFreeBucket;
    pValChain->updatePrimaryBucket();

    m_nNextFreeBucket = nNewFreeBucket;
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
void CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::groupValueTable()
{
    if (getValuesLoadFactor() < getMinLoadFactor())
    {                
        while (m_tableValues.getLastIndex() > m_tableValues.getFirstIndex())
        {
            val_table_type::CChain* pLastValChain = m_tableValues.seekToChain(m_tableValues.getLastIndex());

            if(!pLastValChain->empty())
            {
                break;
            }

            if (m_tableValues.getLastIndex() == m_nNextFreeBucket)
            {
                typename val_table_type::bucket_type* pPrimValBucket = 
                    pLastValChain->getPrimaryBucket();

                m_nNextFreeBucket = pPrimValBucket->nNextFreeBucket;
            }
            m_tableValues.getStorage()->contract();
            m_tableValues.setLastIndex(m_tableValues.getLastIndex() - 1);        
        }
    }
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
size_t CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::size() const
{
    return (spiral_base::getItemsCount() + m_tableValues.getKeysCount());
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
size_t CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::unique() const
{
    return spiral_base::getItemsCount();
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
double CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::getValuesLoadFactor() const
{
    return static_cast<double>(m_tableValues.getKeysCount()) / m_tableValues.getKeysMaxCount();
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
typename CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::const_iterator 
    CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::begin() const
{
    return const_iterator(getItemsEnumerator(), m_tableValues);
}

template <typename HashFunT, typename KeyStorageT, typename ValueStorageT>
typename CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::const_iterator 
    CSpiralDenseMultimapImpl<HashFunT, KeyStorageT, ValueStorageT>::end() const
{
    return const_iterator();
}

template <typename EnumeratorT, typename ValuesTableT>
class CSpiralDenseMultimapConstIterator
{
    typedef EnumeratorT enumerator_type;
    typedef ValuesTableT val_table_type;

    typedef typename enumerator_type::item_type item_type;    
    typedef typename enumerator_type::table_type table_type;
    typedef typename table_type::bucket_type::accessor_type accessor_type;        
    typedef typename val_table_type::CChain val_chain_type;            
public:    
    typedef typename enumerator_type::key_type key_type;       
    typedef typename accessor_type::second_type value_type;

    typedef std::pair<key_type, value_type> value_item_type;
    typedef CSpiralDenseMultimapConstIterator<enumerator_type, val_table_type> const_iterator;

    typedef const value_item_type* const_pointer;
    typedef const value_item_type& const_reference;

    CSpiralDenseMultimapConstIterator()
        : m_pItem(0), m_valueTable(0, 0)
    {
    }

    CSpiralDenseMultimapConstIterator(const item_type* pCurrItem, const val_table_type& table)
        : m_pItem(0), m_pValueChain(0), m_valueTable(table), m_bLastKeyInGroup(false)
    {
        init(pCurrItem);
    }

    CSpiralDenseMultimapConstIterator(const enumerator_type& enumerator, const val_table_type& table)
        : m_pItem(0), m_pValueChain(0), m_enumerator(enumerator), m_valueTable(table), m_bLastKeyInGroup(false)
    {         
        init(m_enumerator.currItem());
    }

    CSpiralDenseMultimapConstIterator(const CSpiralDenseMultimapConstIterator& other);
    CSpiralDenseMultimapConstIterator& operator = (const CSpiralDenseMultimapConstIterator& other);

    const_reference operator*() const
    {
        return m_resultItem;
    }

    const_pointer operator->() const
    {
        return &m_resultItem;
    }

    const_iterator& operator++()
    {
        incrementPosition();
        return *this;
    }

    const_iterator operator++(int)
    {        
        CSpiralDenseMultimapConstIterator prevIter = *this; 
        incrementPosition();
        return prevIter;
    }

    bool operator == (const const_iterator& other)
    {
        return (m_pItem == other.m_pItem);
    }

    bool operator != (const const_iterator& other)
    {        
        return !(*this == other);
    }

    bool isLastKeyInGroup() const
    {
        return m_bLastKeyInGroup;
    }

private:
    const item_type* m_pItem;    
    val_chain_type* m_pValueChain;       
    
    enumerator_type m_enumerator;
    val_table_type m_valueTable;  

    value_item_type m_resultItem;  
    bool m_bLastKeyInGroup;

    void init(const item_type* pItem);
    void seekValueChain();
    void incrementPosition();
};

template <typename EnumeratorT, typename ValuesTableT>
CSpiralDenseMultimapConstIterator<EnumeratorT, ValuesTableT>::CSpiralDenseMultimapConstIterator(const CSpiralDenseMultimapConstIterator& other)
    : m_pItem(0), m_pValueChain(0), m_enumerator(other.m_enumerator), 
    m_valueTable(other.m_valueTable), m_bLastKeyInGroup(other.m_bLastKeyInGroup)
{
    if (!m_enumerator.empty())
    {            
        init(m_enumerator.currItem());
    }
    else
    {
        init(other.m_pItem);
    }
}

template <typename EnumeratorT, typename ValuesTableT>
CSpiralDenseMultimapConstIterator<EnumeratorT, ValuesTableT>& 
    CSpiralDenseMultimapConstIterator<EnumeratorT, ValuesTableT>::operator = (const CSpiralDenseMultimapConstIterator& other)
{    
    m_enumerator = other.m_enumerator;
    m_valueTable = other.m_valueTable;
    m_bLastKeyInGroup = other.m_bLastKeyInGroup;

    if (!m_enumerator.empty())
    {            
        init(m_enumerator.currItem());
    }
    else
    {
        init(other.m_pItem);
    }
    return *this;
}

template <typename EnumeratorT, typename ValuesTableT>
void CSpiralDenseMultimapConstIterator<EnumeratorT, ValuesTableT>::init(const item_type* pItem)
{
    m_pItem = pItem; 
    
    if (0 != m_pItem)
    {
        seekValueChain();
    }
}

template <typename EnumeratorT, typename ValuesTableT>
void CSpiralDenseMultimapConstIterator<EnumeratorT, ValuesTableT>::seekValueChain()
{
    const accessor_type& acsr = m_pItem->second;        
    if (0 != acsr.first)
    {
        m_pValueChain = m_valueTable.seekToChain(acsr.first);            
    }

    m_resultItem.first = m_pItem->first;
    m_resultItem.second = acsr.second;
}

template <typename EnumeratorT, typename ValuesTableT>
void CSpiralDenseMultimapConstIterator<EnumeratorT, ValuesTableT>::incrementPosition()
{
    if (0 != m_pValueChain)
    {
        const val_table_type::item_type* pValueItem = m_pValueChain->nextItem();
        if (0 != pValueItem)
        {
            m_resultItem.second = pValueItem->first;
            m_bLastKeyInGroup = false;
        }
        else
        {
            m_pValueChain = 0;
            m_bLastKeyInGroup = true;
        }            
    }        

    if (0 == m_pValueChain)
    {
        if (m_enumerator.empty())
        {
            m_pItem = 0;
            return;
        }

        m_pItem = m_enumerator.nextItem();

        if (0 != m_pItem)
        {
            seekValueChain();
        }
    }
}

} //namespace algo

#endif // __SPIRALDENSEMULTIMAP_H__