#ifndef __CHAINSTABLE_H__
#define __CHAINSTABLE_H__

#include <assert.h>
#include <set>

namespace algo
{

template <typename StorageT, int cnOverflowDepth>
class CChainsTable
{
public:    
    typedef typename StorageT::bucket_type bucket_type;
    typedef typename bucket_type::hash_key_type hash_key_type;
    typedef typename bucket_type::key_type key_type;
    typedef typename bucket_type::item_type item_type;    
    typedef typename StorageT::CBucketHolder bucket_holder;

    struct ChainBookmark
    {
        int nChainIndex;
        int nOverflowBuckIndex;
        int nSeekKeyIndex;
    };

    class CChain
    {
        friend CChainsTable;

    public:
        //seek for item
        bool seekItem(const hash_key_type& hashKey, const key_type& key);
        int seekLastItem();

        //insertion and erasing
        int insertItem(const hash_key_type& hashKey, const item_type& item, bool bForceChain);        
        template <typename ValueT> 
        void updateItem(const ValueT& value);
        void eraseItem();
        bool empty() const;

        //item enumeration
        const item_type* currItem() const;        
        const item_type* firstItem();
        const item_type* nextItem();
        
        //position bookmarks for sparse iterator
        const ChainBookmark getBookmark() const;
        void setBookmark(const ChainBookmark& bookmark);
        
        //items detach for splitting and grouping
        template <typename F> 
        const item_type* detachNextItem(const F& hashFunc, hash_key_type& hashKey);

        //access to primary bucket
        bucket_type* getPrimaryBucket();
        void updatePrimaryBucket();

        //move foreign keys from primary bucket to other place
        bool moveForeignKeys(); 

        //relink foreign keys to the first or last chain of the table
        void relinkForeignKeys(int nToBuckIndex);  

        //returns items count in chain
        unsigned int calculateItemsCount();
    
    private:
        CChainsTable* m_pTable;

        bucket_holder m_currBucket;
        bucket_holder m_overflowBucket;

        unsigned int m_nCurrBuckIndex;
        int m_nSeekKeyIndex;

        CChain();
        CChain(const CChain&) {}
        CChain& operator = (const CChain& other) { return *this; }

        void seekFirst(CChainsTable* pTable, int nBuckIndex);        
        template <typename F> const item_type* processNextItem(bool bClearChain, F processFunc);
        int insertPrimaryBucket(const hash_key_type& hashKey, const item_type& item, bool bForceChain);
        int insertOverflowBucket(const hash_key_type& hashKey, const item_type& item, bool bForceChain);
        bucket_holder pullupForeignItems(unsigned int nBucIndex, bucket_holder& bucket, 
            const item_type* pAddItem, const hash_key_type& hashKey, unsigned int& nPullBuckIndex, bool bForceChain);
        void detachBucket(unsigned int nUnlinkBuckIndex, bucket_holder& bucketUnlink, 
            unsigned int nLinkBuckIndex, bucket_holder& bucketLink);
        void updateLastBucketIndex(const bucket_holder& bucketChained, int nLastIndex, int nNewLastIndex);
        void erasePrimaryBucket();
        void eraseOverflowBucket();
        void cleanUpBucketOverflow(int nBuckIndex, bucket_holder& bucket);
        void cleanUpBucketPrimary(int nBuckIndex, bucket_holder& bucket);
        void swap(CChain& other);
    };    

public:
    typedef int (*IndexMapFunc)(int nLogicalIndex);

    CChainsTable(StorageT* pStorage, IndexMapFunc mapFunc);
    CChainsTable(const CChainsTable& other);    
    CChainsTable& operator = (const CChainsTable& other);

    void setStorage(StorageT* pStorage);
    StorageT* getStorage() const;

    CChain* seekToChain(int nIndex);    
    CChain* getCurrentChain();
    const CChain* getCurrentChain() const;
    
    const item_type* nextBucketItem();
    const item_type* currBucketItem() const;   

    int getFirstIndex() const;
    int getLastIndex() const;
    void setFirstIndex(int nIndex);
    void setLastIndex(int nIndex);

    int getKeysMaxCount() const;
    int getKeysCount() const;

    void reset(); //turn table to initial state and release buckets
    void clear(); //reset table and cleanup data
    void swap(CChainsTable<StorageT, cnOverflowDepth>& other);

private:
    struct EnumItemProcessFunctor
    {
        const item_type* operator() (const bucket_holder& bucket, int nItemIndex) const
        {
            return &bucket->items[nItemIndex];            
        }
    };

    template <typename F>
    struct DetachItemProcessFunctor
    {    
        DetachItemProcessFunctor(const F& f, hash_key_type& h) : hashFunc(f), hashKey(h) {}        
        const item_type* operator() (bucket_holder& bucket, int nItemIndex)
        {
            const item_type* pItem = &bucket->items[nItemIndex];
            hashKey = bucket->chain.keys.obtainHashKey(hashFunc, pItem->first, nItemIndex);
            return pItem;
        }
        const F& hashFunc;
        hash_key_type& hashKey;
    };

    int m_nFirstBuckIndex;
    int m_nLastBuckIndex;
    int m_nKeysCount;

    IndexMapFunc m_indexFunc;
    StorageT* m_pStorage;    
    
    CChain m_currChain;
    int m_nCurrChainIndex;
    int m_nCurrItemIndex;    

    int m_arrOverflowMap[cnOverflowDepth];

    bucket_holder getBucket(int nBuckIndex);
    void flushBucket(const bucket_holder& bucket, int nBuckIndex);
    unsigned int getCurrChainIndex() const { return m_nCurrChainIndex; }
    bucket_holder searchOverflowBucket(bool bOverall, int nKeys, unsigned int& nBuckIndex);        
    void setKeysCount(int nCount);    
    void putPotentialFreeBucket(int nBuckIndex, bool bForceOut);    
};

template <typename StorageT, int cnOverflowDepth>
CChainsTable<StorageT, cnOverflowDepth>::CChain::CChain()
    : m_pTable(0), m_currBucket(), m_overflowBucket(), 
    m_nSeekKeyIndex(-1), m_nCurrBuckIndex(0)
{
}

template <typename StorageT, int cnOverflowDepth>
bool CChainsTable<StorageT, cnOverflowDepth>::CChain::seekItem(const hash_key_type& hashKey, const key_type& key)
{        
    if (0 == m_overflowBucket.get())
    {
        for (int i = m_nSeekKeyIndex + 1; i < m_currBucket->chain.nCurrentItem; ++i)
        {
            if (m_currBucket->chain.keys.compare(hashKey, i) && 
                key == m_currBucket->items[i].first)
            {
                m_overflowBucket = bucket_holder(); 
                m_nCurrBuckIndex = m_pTable->getCurrChainIndex();
                m_nSeekKeyIndex = i;
                return true;
            }
        }

        if (0 != m_currBucket->chain.nPrimNextIndex)
        {               
            m_nCurrBuckIndex = m_currBucket->chain.nPrimNextIndex;
            m_overflowBucket = m_pTable->getBucket(m_nCurrBuckIndex);
            m_nSeekKeyIndex = bucket_type::cnBucketLength;
        }
    }

    while (0 != m_overflowBucket.get())
    {
        assert(m_overflowBucket->chain.nPrevIndex != 0);

        for (int i = m_nSeekKeyIndex - 1; 
            i >= (bucket_type::cnBucketLength - m_overflowBucket->chain.nForeignItems); --i)
        {            
            if (m_overflowBucket->chain.keys.compare(hashKey, i) && 
                key == m_overflowBucket->items[i].first)
            {
                m_nSeekKeyIndex = i;
                return true;
            }            
        }

        if (0 != m_overflowBucket->chain.nFgNextIndex)
        {
            m_nCurrBuckIndex = m_overflowBucket->chain.nFgNextIndex;
            m_overflowBucket = m_pTable->getBucket(m_nCurrBuckIndex);
            m_nSeekKeyIndex = bucket_type::cnBucketLength;
        }
        else
        {
            break;
        }
    }

    return false;
}

template <typename StorageT, int cnOverflowDepth>
int CChainsTable<StorageT, cnOverflowDepth>::CChain::seekLastItem()
{  
    if (0 != m_currBucket->chain.nPrimNextIndex)
    {        
        m_nCurrBuckIndex = m_currBucket->chain.nLastChainIndex;
        m_overflowBucket = m_pTable->getBucket(m_nCurrBuckIndex);
    }

    return m_nCurrBuckIndex;
}

template <typename StorageT, int cnOverflowDepth>
const typename CChainsTable<StorageT, cnOverflowDepth>::item_type* 
    CChainsTable<StorageT, cnOverflowDepth>::CChain::currItem() const
{
    assert(-1 != m_nSeekKeyIndex);

    if (0 != m_overflowBucket.get())
    {
        return &m_overflowBucket->items[m_nSeekKeyIndex];
    }
   
    return &m_currBucket->items[m_nSeekKeyIndex];
}

template <typename StorageT, int cnOverflowDepth>
const typename CChainsTable<StorageT, cnOverflowDepth>::item_type* 
    CChainsTable<StorageT, cnOverflowDepth>::CChain::firstItem()
{
    seekFirst(m_pTable, 0);    
    return nextItem();
}

template <typename StorageT, int cnOverflowDepth>
const typename CChainsTable<StorageT, cnOverflowDepth>::item_type* 
    CChainsTable<StorageT, cnOverflowDepth>::CChain::nextItem()
{    
    return processNextItem(false, EnumItemProcessFunctor());
}

template <typename StorageT, int cnOverflowDepth>
const typename CChainsTable<StorageT, cnOverflowDepth>::ChainBookmark 
    CChainsTable<StorageT, cnOverflowDepth>::CChain::getBookmark() const
{
    ChainBookmark bookmark;
    bookmark.nChainIndex = m_pTable->getCurrChainIndex();
    bookmark.nOverflowBuckIndex = (0 == m_overflowBucket.get()) ? 0 : m_nCurrBuckIndex;
    bookmark.nSeekKeyIndex = m_nSeekKeyIndex;
    return bookmark;
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::setBookmark(const ChainBookmark& bookmark)
{
    assert(m_pTable->getCurrChainIndex() == bookmark.nChainIndex);

    if (0 != bookmark.nOverflowBuckIndex)
    {   
        m_nCurrBuckIndex = bookmark.nOverflowBuckIndex;
        m_overflowBucket = m_pTable->getBucket(m_nCurrBuckIndex);
    }
    else
    {
        m_nCurrBuckIndex = m_pTable->getCurrChainIndex();
        m_overflowBucket = bucket_holder();
    }
    m_nSeekKeyIndex = bookmark.nSeekKeyIndex;
}

template <typename StorageT, int cnOverflowDepth> template <typename F>
const typename CChainsTable<StorageT, cnOverflowDepth>::item_type* 
    CChainsTable<StorageT, cnOverflowDepth>::CChain::detachNextItem(const F& hashFunc, hash_key_type& hashKey)
{    
    DetachItemProcessFunctor<F> func(hashFunc, hashKey);
    return processNextItem(true, func);
}

template <typename StorageT, int cnOverflowDepth> template <typename F>
const typename CChainsTable<StorageT, cnOverflowDepth>::item_type* 
    CChainsTable<StorageT, cnOverflowDepth>::CChain::processNextItem(bool bClearChain, F processFunc)
{
    if (0 != m_overflowBucket.get())
    {
        --m_nSeekKeyIndex;

        if (m_nSeekKeyIndex == (bucket_type::cnBucketLength - m_overflowBucket->chain.nForeignItems - 1))
        {
            const int nFgNextIndex = m_overflowBucket->chain.nFgNextIndex;

            if (bClearChain)
            {                
                cleanUpBucketOverflow(m_nCurrBuckIndex, m_overflowBucket);
                m_overflowBucket = bucket_holder();
            }

            if (0 == nFgNextIndex)
            {
                return 0;
            }

            m_nCurrBuckIndex = nFgNextIndex;
            m_overflowBucket = m_pTable->getBucket(m_nCurrBuckIndex);
            m_nSeekKeyIndex = bucket_type::cnBucketLength - 1;
        }
        
        return processFunc(m_overflowBucket, m_nSeekKeyIndex);
    }

    ++m_nSeekKeyIndex;

    if (m_nSeekKeyIndex == m_currBucket->chain.nCurrentItem)
    {
        const int nPrimNextIndex = m_currBucket->chain.nPrimNextIndex;

        if (bClearChain)
        {
            cleanUpBucketPrimary(m_nCurrBuckIndex, m_currBucket);
        }

        if (0 == nPrimNextIndex)
        {
            return 0;
        }

        m_nCurrBuckIndex = nPrimNextIndex;
        m_overflowBucket = m_pTable->getBucket(m_nCurrBuckIndex);
        m_nSeekKeyIndex = bucket_type::cnBucketLength - 1;            

        return processFunc(m_overflowBucket, m_nSeekKeyIndex);        
    }

    return processFunc(m_currBucket, m_nSeekKeyIndex);        
}

template <typename StorageT, int cnOverflowDepth>
typename CChainsTable<StorageT, cnOverflowDepth>::bucket_type* 
    CChainsTable<StorageT, cnOverflowDepth>::CChain::getPrimaryBucket()
{
    return m_currBucket.get();
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::updatePrimaryBucket()
{
    m_pTable->flushBucket(m_currBucket, m_pTable->getCurrChainIndex());
}

template <typename StorageT, int cnOverflowDepth>
int CChainsTable<StorageT, cnOverflowDepth>::
    CChain::insertItem(const hash_key_type& hashKey, const item_type& item, bool bForceChain)
{
    //last bucket initialized by seekKey function
    int nInsBuckIndex = 0;
    if (0 != m_overflowBucket.get())
    {
        nInsBuckIndex = insertOverflowBucket(hashKey, item, bForceChain);                
    }
    else
    {
        nInsBuckIndex = insertPrimaryBucket(hashKey, item, bForceChain);        
    }

    if (nInsBuckIndex > 0)
    {
        m_pTable->setKeysCount(m_pTable->getKeysCount() + 1);
    }

    return nInsBuckIndex;
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::seekFirst(CChainsTable* pTable, int nBuckIndex)
{    
    if (0 != nBuckIndex)
    {
        m_pTable = pTable;
        m_currBucket = m_pTable->getBucket(nBuckIndex);
    }
    m_overflowBucket = bucket_holder();    
    m_nCurrBuckIndex = m_pTable->getCurrChainIndex();
    m_nSeekKeyIndex = -1;
}

template <typename StorageT, int cnOverflowDepth>
int CChainsTable<StorageT, cnOverflowDepth>::
    CChain::insertPrimaryBucket(const hash_key_type& hashKey, const item_type& item, bool bForceChain)
{
    assert(0 != m_currBucket.get());

    //bucket has empty space
    if (m_currBucket->chain.nCurrentItem < 
        (bucket_type::cnBucketLength - m_currBucket->chain.nForeignItems))
    {
        m_nSeekKeyIndex = m_currBucket->chain.nCurrentItem;
        m_currBucket->chain.keys.insert(hashKey, m_nSeekKeyIndex);
        m_currBucket->items[m_nSeekKeyIndex] = item;
        ++m_currBucket->chain.nCurrentItem;

        m_pTable->flushBucket(m_currBucket, m_nCurrBuckIndex);
    }        
    else 
    {        
        if (0 != m_currBucket->chain.nForeignItems) //bucket has foreign keys
        {            
            unsigned int nPullBuckIndex = 0;
            bucket_holder pullBucket = pullupForeignItems(m_nCurrBuckIndex, m_currBucket, 
                0, 0, nPullBuckIndex, bForceChain);

            if (0 != pullBucket.get())
            {
                m_nSeekKeyIndex = m_currBucket->chain.nCurrentItem;
                m_currBucket->chain.keys.insert(hashKey, m_nSeekKeyIndex);
                m_currBucket->items[m_nSeekKeyIndex] = item;
                ++m_currBucket->chain.nCurrentItem;

                m_pTable->flushBucket(pullBucket, nPullBuckIndex);
                m_pTable->flushBucket(m_currBucket, m_nCurrBuckIndex);            

                return m_nCurrBuckIndex;
            }
        }        

        //search bucket for overflow
        unsigned int nOverflowBuckIndex = 0;
        bucket_holder bucket = m_pTable->searchOverflowBucket(bForceChain, 1, nOverflowBuckIndex);
        if (0 == bucket.get())
        {
            return -1; //not enough space in table
        }

        //link overflow bucket with primary bucket
        assert(m_nCurrBuckIndex != nOverflowBuckIndex);
        bucket->chain.nPrevIndex = m_nCurrBuckIndex;                            
        m_currBucket->chain.nPrimNextIndex = nOverflowBuckIndex;        
        m_currBucket->chain.nLastChainIndex = nOverflowBuckIndex;
        
        assert(0 == bucket->chain.nForeignItems);
        m_nSeekKeyIndex = bucket_type::cnBucketLength - 1;
        bucket->chain.keys.insert(hashKey, m_nSeekKeyIndex);
        bucket->items[m_nSeekKeyIndex] = item;
        ++bucket->chain.nForeignItems;        
        
        //flush buckets data
        m_pTable->flushBucket(m_currBucket, m_nCurrBuckIndex);
        m_pTable->flushBucket(bucket, nOverflowBuckIndex);                       

        m_overflowBucket = bucket;        
        m_nCurrBuckIndex = nOverflowBuckIndex;
    }

    return m_nCurrBuckIndex;
}

template <typename StorageT, int cnOverflowDepth>
int CChainsTable<StorageT, cnOverflowDepth>::
    CChain::insertOverflowBucket(const hash_key_type& hashKey, const item_type& item, bool bForceChain)
{    
    if (m_overflowBucket->chain.nForeignItems < 
        (bucket_type::cnBucketLength - m_overflowBucket->chain.nCurrentItem))
    {
        m_nSeekKeyIndex = bucket_type::cnBucketLength - m_overflowBucket->chain.nForeignItems - 1;
        m_overflowBucket->chain.keys.insert(hashKey, m_nSeekKeyIndex);
        m_overflowBucket->items[m_nSeekKeyIndex] = item;
        ++m_overflowBucket->chain.nForeignItems;

        m_pTable->flushBucket(m_overflowBucket, m_nCurrBuckIndex);
    }
    else
    {
        unsigned int nPullBuckIndex = 0;
        bucket_holder pullBucket = pullupForeignItems(m_nCurrBuckIndex, m_overflowBucket, 
            &item, hashKey, nPullBuckIndex, bForceChain);

        if (0 != pullBucket.get())
        {                        
            m_pTable->flushBucket(m_overflowBucket, m_nCurrBuckIndex);
            m_pTable->flushBucket(pullBucket, nPullBuckIndex);

            if (0 != pullBucket->chain.nPrevIndex)
            {
                m_overflowBucket = pullBucket;
            }
            else
            {
                m_currBucket = pullBucket;
                m_overflowBucket = bucket_holder();
            }

            m_nCurrBuckIndex = nPullBuckIndex;
        }
        else
        {            
            unsigned int nOverflowBuckIndex = 0;
            bucket_holder bucket = m_pTable->searchOverflowBucket(bForceChain, 1, nOverflowBuckIndex);
            if (0 == bucket.get())
            {
                return -1; //not enough space in table
            }

            //link overflow bucket with primary bucket
            assert(nOverflowBuckIndex != m_nCurrBuckIndex);
            bucket->chain.nPrevIndex = m_nCurrBuckIndex;            
            m_overflowBucket->chain.nFgNextIndex = nOverflowBuckIndex;

            assert(0 == bucket->chain.nForeignItems);
            m_nSeekKeyIndex = bucket_type::cnBucketLength - 1;
            bucket->chain.keys.insert(hashKey, m_nSeekKeyIndex);
            bucket->items[m_nSeekKeyIndex] = item;
            ++bucket->chain.nForeignItems;

            m_currBucket->chain.nLastChainIndex = nOverflowBuckIndex;

            //flush buckets data
            m_pTable->flushBucket(m_overflowBucket, m_nCurrBuckIndex);
            m_pTable->flushBucket(bucket, nOverflowBuckIndex);
            m_pTable->flushBucket(m_currBucket, m_pTable->getCurrChainIndex());

            m_overflowBucket = bucket;
            m_nCurrBuckIndex = nOverflowBuckIndex;  
        }
    }

    return m_nCurrBuckIndex;
}


template <typename StorageT, int cnOverflowDepth> template <typename ValueT>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::updateItem(const ValueT& value)
{
    assert(-1 != m_nSeekKeyIndex);

    if (0 != m_overflowBucket.get())
    {
        m_overflowBucket->items[m_nSeekKeyIndex].second = value;
        m_pTable->flushBucket(m_overflowBucket, m_nCurrBuckIndex);        
    }
    else
    {
        m_currBucket->items[m_nSeekKeyIndex].second = value;    
        m_pTable->flushBucket(m_currBucket, m_nCurrBuckIndex);
    }
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::eraseItem()
{    
    if (0 != m_overflowBucket.get())
    {
        eraseOverflowBucket();        
    }
    else
    {
        erasePrimaryBucket();        
    }

    m_pTable->setKeysCount(m_pTable->getKeysCount() - 1);    
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::erasePrimaryBucket()
{
    //if bucket has no overflow
    if (0 == m_currBucket->chain.nPrimNextIndex)
    {
        //move last key to hole, fake moving for once key in bucket
        const int nLastKeyIndex = m_currBucket->chain.nCurrentItem - 1;
        m_currBucket->chain.keys.move(m_nSeekKeyIndex, m_currBucket->chain.keys, nLastKeyIndex);
        m_currBucket->items[m_nSeekKeyIndex] = m_currBucket->items[nLastKeyIndex];
        --m_currBucket->chain.nCurrentItem;
    }
    else
    {
        //get last bucket in the chain        
        const int nLastBuckIndex = m_currBucket->chain.nLastChainIndex;        
        bucket_holder lastBucket = m_pTable->getBucket(nLastBuckIndex);

        //move last key from last bucket to primary bucket
        const int nLastKeyIndex = bucket_type::cnBucketLength - lastBucket->chain.nForeignItems;
        m_currBucket->chain.keys.move(m_nSeekKeyIndex, lastBucket->chain.keys, nLastKeyIndex);
        m_currBucket->items[m_nSeekKeyIndex] = lastBucket->items[nLastKeyIndex];
        --lastBucket->chain.nForeignItems;

        //if overflow bucket has no keys, then unlink bucket from chain
        if (0 == lastBucket->chain.nForeignItems)
        {            
            m_pTable->putPotentialFreeBucket(nLastBuckIndex, true);

            if (lastBucket->chain.nPrevIndex == m_pTable->getCurrChainIndex())
            {
                m_currBucket->chain.nPrimNextIndex = 0;
                m_currBucket->chain.nLastChainIndex = 0;
            }
            else
            {
                m_currBucket->chain.nLastChainIndex = lastBucket->chain.nPrevIndex;

                bucket_holder referBucket = m_pTable->getBucket(lastBucket->chain.nPrevIndex);
                referBucket->chain.nFgNextIndex = 0;
                m_pTable->flushBucket(referBucket, lastBucket->chain.nPrevIndex);
            }            
            lastBucket->chain.nPrevIndex = 0;       
        }

        m_pTable->flushBucket(lastBucket, nLastBuckIndex);
    }

    m_pTable->flushBucket(m_currBucket, m_nCurrBuckIndex);    
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::eraseOverflowBucket()
{    
    //if bucket last in the chain
    if (0 == m_overflowBucket->chain.nFgNextIndex)
    {
        //move last key, fake moving for once overflow key
        const int nLastKeyIndex = bucket_type::cnBucketLength - m_overflowBucket->chain.nForeignItems;
        m_overflowBucket->chain.keys.move(m_nSeekKeyIndex, m_overflowBucket->chain.keys, nLastKeyIndex);
        m_overflowBucket->items[m_nSeekKeyIndex] = m_overflowBucket->items[nLastKeyIndex];
        --m_overflowBucket->chain.nForeignItems;

        //if overflow bucket last in the chain and has empty overflow
        if (0 == m_overflowBucket->chain.nForeignItems)
        {
            assert(0 != m_overflowBucket->chain.nPrevIndex);                        
            const unsigned int nReferIndex = m_overflowBucket->chain.nPrevIndex;
            m_overflowBucket->chain.nPrevIndex = 0;                     
            m_pTable->putPotentialFreeBucket(m_nCurrBuckIndex, true);

            if (nReferIndex == m_pTable->getCurrChainIndex())
            {
                m_currBucket->chain.nPrimNextIndex = 0;
                m_currBucket->chain.nLastChainIndex = 0;

                m_pTable->flushBucket(m_currBucket, nReferIndex);
                m_pTable->flushBucket(m_overflowBucket, m_nCurrBuckIndex);

                m_nCurrBuckIndex = nReferIndex;
                m_overflowBucket = bucket_holder();

                return; //reset to primary bucket
            }
            else 
            {
                bucket_holder referBucket = m_pTable->getBucket(nReferIndex);

                assert(referBucket->chain.nPrimNextIndex == m_nCurrBuckIndex ||
                    referBucket->chain.nFgNextIndex == m_nCurrBuckIndex);

                referBucket->chain.nFgNextIndex = 0;                
                m_pTable->flushBucket(referBucket, nReferIndex);

                //decrease chain length counter
                m_currBucket->chain.nLastChainIndex = nReferIndex;
                m_pTable->flushBucket(m_currBucket, m_pTable->getCurrChainIndex());
            }            
        }        
    }
    else
    {
        //get last bucket in the chain        
        const int nLastBuckIndex = m_currBucket->chain.nLastChainIndex;
        bucket_holder lastBucket = m_pTable->getBucket(nLastBuckIndex);        

        //move last key from last bucket
        const int nLastKeyIndex = bucket_type::cnBucketLength - lastBucket->chain.nForeignItems;
        m_overflowBucket->chain.keys.move(m_nSeekKeyIndex, lastBucket->chain.keys, nLastKeyIndex);
        m_overflowBucket->items[m_nSeekKeyIndex] = lastBucket->items[nLastKeyIndex];
        --lastBucket->chain.nForeignItems;

        //if overflow empty, then unlink bucket from chain
        if (0 == lastBucket->chain.nForeignItems)
        {            
            m_pTable->putPotentialFreeBucket(nLastBuckIndex, true);
            
            if (lastBucket->chain.nPrevIndex == m_nCurrBuckIndex)
            {                
                m_overflowBucket->chain.nFgNextIndex = 0;
            }
            else
            {
                bucket_holder referBucket = m_pTable->getBucket(lastBucket->chain.nPrevIndex);
                referBucket->chain.nFgNextIndex = 0;
                m_pTable->flushBucket(referBucket, lastBucket->chain.nPrevIndex);
            }

            m_currBucket->chain.nLastChainIndex = lastBucket->chain.nPrevIndex;
            lastBucket->chain.nPrevIndex = 0;

            m_pTable->flushBucket(m_currBucket, m_pTable->getCurrChainIndex());
        }

        m_pTable->flushBucket(lastBucket, nLastBuckIndex);
    }    

    m_pTable->flushBucket(m_overflowBucket, m_nCurrBuckIndex);    
}

template <typename StorageT, int cnOverflowDepth>
bool CChainsTable<StorageT, cnOverflowDepth>::CChain::empty() const
{
    return (0 == m_currBucket->chain.nCurrentItem && 
            0 == m_currBucket->chain.nPrimNextIndex &&
            0 == m_currBucket->chain.nForeignItems);
}

template <typename StorageT, int cnOverflowDepth>
bool CChainsTable<StorageT, cnOverflowDepth>::CChain::moveForeignKeys()
{
    if (0 != m_currBucket->chain.nForeignItems)
    {
        unsigned int nPullBuckIndex = 0;
        bucket_holder pullBucket = pullupForeignItems(m_pTable->getCurrChainIndex(), 
            m_currBucket, 0, 0, nPullBuckIndex, false);

        if (0 == pullBucket.get())
            return false;

        m_pTable->flushBucket(pullBucket, nPullBuckIndex);
        m_pTable->flushBucket(m_currBucket, m_pTable->getCurrChainIndex());
        
        m_pTable->putPotentialFreeBucket(m_pTable->getCurrChainIndex(), true);
    }

    return true;
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::relinkForeignKeys(int nToBuckIndex)
{
    if (0 != m_currBucket->chain.nForeignItems)
    {
        assert(0 != m_currBucket->chain.nPrevIndex);    
        
        //update previous bucket in chain
        bucket_holder referBucket = m_pTable->getBucket(m_currBucket->chain.nPrevIndex);        
        if (referBucket->chain.nPrimNextIndex == m_pTable->getCurrChainIndex())
        {
            referBucket->chain.nPrimNextIndex = nToBuckIndex;
            if (0 == m_currBucket->chain.nFgNextIndex)
            {
                referBucket->chain.nLastChainIndex = nToBuckIndex;
            }
        }
        else
        {
            referBucket->chain.nFgNextIndex = nToBuckIndex;
            if (0 == m_currBucket->chain.nFgNextIndex)
            {
                updateLastBucketIndex(referBucket, m_pTable->getCurrChainIndex(), nToBuckIndex);                    
            }
        }
        m_pTable->flushBucket(referBucket, m_currBucket->chain.nPrevIndex);

        //update next bucket in chain
        if (0 != m_currBucket->chain.nFgNextIndex)
        {
            bucket_holder nextBucket = m_pTable->getBucket(m_currBucket->chain.nFgNextIndex);
            assert(0 != nextBucket->chain.nPrevIndex);
            nextBucket->chain.nPrevIndex = nToBuckIndex;
            m_pTable->flushBucket(nextBucket, m_currBucket->chain.nFgNextIndex);
        }
    }
}

template <typename StorageT, int cnOverflowDepth>
unsigned int CChainsTable<StorageT, cnOverflowDepth>::CChain::calculateItemsCount()
{
    unsigned int nItemsCount = m_currBucket->chain.nCurrentItem;    
    int nNextBuckIndex = m_currBucket->chain.nPrimNextIndex;

    while (0 != nNextBuckIndex)
    {
        bucket_holder bucket = m_pTable->getBucket(nNextBuckIndex);
        nItemsCount += bucket->chain.nForeignItems;
        nNextBuckIndex = bucket->chain.nFgNextIndex;
    }

    return nItemsCount;
}

template <typename StorageT, int cnOverflowDepth>
typename CChainsTable<StorageT, cnOverflowDepth>::bucket_holder CChainsTable<StorageT, cnOverflowDepth>::
    CChain::pullupForeignItems(unsigned int nBuckIndex, bucket_holder& bucket, 
        const item_type* pAddItem, const hash_key_type& hashKey, unsigned int& nPullBuckIndex, bool bForceChain)
{    
    const unsigned int nFreeKeys = 
        (0 == pAddItem) ? bucket->chain.nForeignItems : bucket->chain.nForeignItems + 1;

    if (nFreeKeys > bucket_type::cnBucketLength)
        return bucket_holder();

    //search bucket to move overflow keys  
    bucket_holder pullBucket = m_pTable->searchOverflowBucket(bForceChain, nFreeKeys, nPullBuckIndex);
    if (0 == pullBucket.get())
        return bucket_holder();

    assert(nBuckIndex != nPullBuckIndex);

    //if found bucket is not primary bucket of the chain
    if (bucket->chain.nPrevIndex != nPullBuckIndex)
    {    
        //move keys to other bucket
        int nForeignInd = bucket_type::cnBucketLength - 1;
        while (nForeignInd >= (bucket_type::cnBucketLength - bucket->chain.nForeignItems))
        {
            pullBucket->chain.keys.move(nForeignInd, bucket->chain.keys, nForeignInd);
            pullBucket->items[nForeignInd] = bucket->items[nForeignInd];
            --nForeignInd;
        }

        pullBucket->chain.nForeignItems = bucket->chain.nForeignItems;
        bucket->chain.nForeignItems = 0;

        if (0 != pAddItem)
        {
            m_nSeekKeyIndex = nForeignInd;
            pullBucket->chain.keys.insert(hashKey, nForeignInd);
            pullBucket->items[nForeignInd] = *pAddItem;
            ++pullBucket->chain.nForeignItems;
        }                        

        //relink new bucket in chain
        detachBucket(nBuckIndex, bucket, nPullBuckIndex, pullBucket);    
    }
    else
    {
        //reform primary bucket
        int nForeignInd = bucket_type::cnBucketLength - 1;
        while (nForeignInd >= (bucket_type::cnBucketLength - bucket->chain.nForeignItems))
        {
            const int nCurrItem = pullBucket->chain.nCurrentItem;
            pullBucket->chain.keys.move(nCurrItem, bucket->chain.keys, nForeignInd);
            pullBucket->items[nCurrItem] = bucket->items[nForeignInd];            
            --nForeignInd;
            ++pullBucket->chain.nCurrentItem;
        }

        if (0 != pAddItem)
        {
            m_nSeekKeyIndex = pullBucket->chain.nCurrentItem;
            pullBucket->chain.keys.insert(hashKey, m_nSeekKeyIndex);
            pullBucket->items[m_nSeekKeyIndex] = *pAddItem;
            ++pullBucket->chain.nCurrentItem;
        }

        //unlink overflow bucket
        assert(pullBucket->chain.nPrimNextIndex == nBuckIndex);

        pullBucket->chain.nPrimNextIndex = 0;
        pullBucket->chain.nLastChainIndex = 0;
        pullBucket->chain.nPrimNextIndex = bucket->chain.nFgNextIndex;

        bucket->chain.nForeignItems = 0;
        bucket->chain.nPrevIndex = 0;
        bucket->chain.nFgNextIndex = 0;
    }    

    return pullBucket;
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::cleanUpBucketOverflow(int nBuckIndex, bucket_holder& bucket)
{
    m_pTable->setKeysCount(m_pTable->getKeysCount() - bucket->chain.nForeignItems);

    bucket->chain.nForeignItems = 0;
    bucket->chain.nPrevIndex = 0;
    bucket->chain.nFgNextIndex = 0;

    m_pTable->flushBucket(bucket, nBuckIndex);
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::cleanUpBucketPrimary(int nBuckIndex, bucket_holder& bucket)
{
    m_pTable->setKeysCount(m_pTable->getKeysCount() - bucket->chain.nCurrentItem);    

    bucket->chain.nCurrentItem = 0;
    bucket->chain.nPrimNextIndex = 0;
    bucket->chain.nLastChainIndex = 0;

    m_pTable->flushBucket(bucket, nBuckIndex);
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::CChain::swap(CChain& other)
{
    std::swap(m_pTable, other.m_pTable);
    std::swap(m_nCurrBuckIndex, other.m_nCurrBuckIndex);
    std::swap(m_nSeekKeyIndex, other.m_nSeekKeyIndex);

    m_currBucket.swap(other.m_currBucket);
    m_overflowBucket.swap(other.m_overflowBucket);
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::
    CChain::detachBucket(unsigned int nUnlinkBuckIndex, bucket_holder& bucketUnlink, 
                    unsigned int nLinkBuckIndex, bucket_holder& bucketLink)
{
    //relink previous bucket in chain
    assert(0 != bucketUnlink->chain.nPrevIndex);

    if (m_pTable->getCurrChainIndex() == bucketUnlink->chain.nPrevIndex)
    {
        m_currBucket->chain.nPrimNextIndex = nLinkBuckIndex;
        
        if (nUnlinkBuckIndex == m_currBucket->chain.nLastChainIndex)
            m_currBucket->chain.nLastChainIndex = nLinkBuckIndex;

        m_pTable->flushBucket(m_currBucket, m_pTable->getCurrChainIndex());
    }
    else
    {
        bucket_holder referBucket = m_pTable->getBucket(bucketUnlink->chain.nPrevIndex);  

        assert(referBucket->chain.nPrimNextIndex == nUnlinkBuckIndex ||
            referBucket->chain.nFgNextIndex == nUnlinkBuckIndex);

        if (referBucket->chain.nPrimNextIndex == nUnlinkBuckIndex)
        {
            //update link to last chain
            if (0 == bucketUnlink->chain.nFgNextIndex)
            {
                referBucket->chain.nLastChainIndex = nLinkBuckIndex;
            }

            //update bucket link
            referBucket->chain.nPrimNextIndex = nLinkBuckIndex;            
        }
        else
        {            
            //update link to last chain
            if (0 == bucketUnlink->chain.nFgNextIndex)
            {
                updateLastBucketIndex(referBucket, nUnlinkBuckIndex, nLinkBuckIndex);                
            }     

            //update bucket link
            referBucket->chain.nFgNextIndex = nLinkBuckIndex;
        }

        m_pTable->flushBucket(referBucket, bucketUnlink->chain.nPrevIndex);        
    }

    //relink next bucket in chain
    if (0 != bucketUnlink->chain.nFgNextIndex)
    {
        bucket_holder nextBucket = m_pTable->getBucket(bucketUnlink->chain.nFgNextIndex);
        nextBucket->chain.nPrevIndex = nLinkBuckIndex;
        m_pTable->flushBucket(nextBucket, bucketUnlink->chain.nFgNextIndex);
    }

    bucketLink->chain.nPrevIndex = bucketUnlink->chain.nPrevIndex;    
    bucketLink->chain.nFgNextIndex = bucketUnlink->chain.nFgNextIndex;      
    bucketUnlink->chain.nPrevIndex = 0;
    bucketUnlink->chain.nFgNextIndex = 0;
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::
    CChain::updateLastBucketIndex(const bucket_holder& bucketChained, int nLastIndex, int nNewLastIndex)
{
    int nPrimBuckIndex = bucketChained->chain.nPrevIndex;    
    if (m_pTable->getCurrChainIndex() != nPrimBuckIndex)
    {            
        bucket_holder primBucket = m_pTable->getBucket(nPrimBuckIndex);

        while (primBucket->chain.nLastChainIndex != nLastIndex)
        {                    
            nPrimBuckIndex = primBucket->chain.nPrevIndex;
            
            if (m_pTable->getCurrChainIndex() == nPrimBuckIndex)
            {
                m_currBucket->chain.nLastChainIndex = nNewLastIndex;
                m_pTable->flushBucket(m_currBucket, m_pTable->getCurrChainIndex());
                return;
            }
            
            primBucket = m_pTable->getBucket(nPrimBuckIndex);
        }

        primBucket->chain.nLastChainIndex = nNewLastIndex;
        m_pTable->flushBucket(primBucket, nPrimBuckIndex);
    }
    else
    {
        m_currBucket->chain.nLastChainIndex = nNewLastIndex;
        m_pTable->flushBucket(m_currBucket, m_pTable->getCurrChainIndex());
    }
}

template <typename StorageT, int cnOverflowDepth>
CChainsTable<StorageT, cnOverflowDepth>::CChainsTable(StorageT* pStorage, IndexMapFunc mapFunc)
    : m_pStorage(pStorage), m_indexFunc(mapFunc), 
    m_nFirstBuckIndex(1), m_nLastBuckIndex(1), 
    m_nKeysCount(0), m_nCurrChainIndex(0), m_nCurrItemIndex(0)    
{   
    for (int nMapInd = 0; nMapInd < cnOverflowDepth; ++nMapInd)    
        m_arrOverflowMap[nMapInd] = 0;
    
    putPotentialFreeBucket(m_nLastBuckIndex, true);
}   

template <typename StorageT, int cnOverflowDepth>
CChainsTable<StorageT, cnOverflowDepth>::CChainsTable(const CChainsTable& other)
    : m_pStorage(other.m_pStorage), m_indexFunc(other.m_indexFunc), 
    m_nFirstBuckIndex(other.m_nFirstBuckIndex),  m_nLastBuckIndex(other.m_nLastBuckIndex), 
    m_nKeysCount(other.m_nKeysCount), m_nCurrChainIndex(other.m_nCurrChainIndex), m_nCurrItemIndex(other.m_nCurrItemIndex)    
{
    for (int nMapInd = 0; nMapInd < cnOverflowDepth; ++nMapInd)    
        m_arrOverflowMap[nMapInd] = other.m_arrOverflowMap[nMapInd];

    if (0 != m_nCurrChainIndex)
    {    
        m_currChain.seekFirst(this, m_nCurrChainIndex);
    }
}

template <typename StorageT, int cnOverflowDepth>
CChainsTable<StorageT, cnOverflowDepth>& CChainsTable<StorageT, cnOverflowDepth>::operator = (const CChainsTable& other)
{
    m_pStorage = other.m_pStorage;
    m_indexFunc = other.m_indexFunc;    
    m_nFirstBuckIndex = other.m_nFirstBuckIndex;
    m_nLastBuckIndex = other.m_nLastBuckIndex;
    m_nKeysCount = other.m_nKeysCount;
    m_nCurrChainIndex = other.m_nCurrChainIndex;
    m_nCurrItemIndex = other.m_nCurrItemIndex;
    
    for (int nMapInd = 0; nMapInd < cnOverflowDepth; ++nMapInd)    
        m_arrOverflowMap[nMapInd] = other. m_arrOverflowMap[nMapInd];

    if (0 != m_nCurrChainIndex)
    {
        m_currChain.seekFirst(this, m_nCurrChainIndex);
    }
    return *this;
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::setStorage(StorageT* pStorage)
{
    m_pStorage = pStorage;
}

template <typename StorageT, int cnOverflowDepth>
StorageT* CChainsTable<StorageT, cnOverflowDepth>::getStorage() const
{
    return m_pStorage;
}

template <typename StorageT, int cnOverflowDepth>
typename CChainsTable<StorageT, cnOverflowDepth>::CChain* 
    CChainsTable<StorageT, cnOverflowDepth>::seekToChain(int nIndex)
{
    assert(nIndex >= m_nFirstBuckIndex);
    assert(nIndex <= m_nLastBuckIndex);

    if (m_nCurrChainIndex != nIndex)
    {
        //read chain
        m_nCurrChainIndex = nIndex;
        m_currChain.seekFirst(this, nIndex);
    }
    else
    {
        //reset current chain
        m_currChain.seekFirst(0, 0);
    }

    m_nCurrItemIndex = 0;
    return &m_currChain;
}

template <typename StorageT, int cnOverflowDepth>
typename CChainsTable<StorageT, cnOverflowDepth>::CChain* 
    CChainsTable<StorageT, cnOverflowDepth>::getCurrentChain()
{
    assert(0 != m_nCurrChainIndex);
    return &m_currChain;
}

template <typename StorageT, int cnOverflowDepth>
typename const CChainsTable<StorageT, cnOverflowDepth>::CChain* 
    CChainsTable<StorageT, cnOverflowDepth>::getCurrentChain() const
{
    assert(0 != m_nCurrChainIndex);
    return &m_currChain;
}


template <typename StorageT, int cnOverflowDepth>
const typename CChainsTable<StorageT, cnOverflowDepth>::item_type* 
    CChainsTable<StorageT, cnOverflowDepth>::nextBucketItem()
{   
    ++m_nCurrItemIndex;
    return currBucketItem();    
}

template <typename StorageT, int cnOverflowDepth>
const typename CChainsTable<StorageT, cnOverflowDepth>::item_type* 
    CChainsTable<StorageT, cnOverflowDepth>::currBucketItem() const
{
    const bucket_holder& bucket = m_currChain.m_currBucket;

    if (m_nCurrItemIndex < bucket->chain.nCurrentItem)
    {
        return &bucket->items[m_nCurrItemIndex];
    }
    else
    {        
        const int nFgItems = (m_nCurrItemIndex - bucket->chain.nCurrentItem);

        if (nFgItems < bucket->chain.nForeignItems)
        {            
            return &bucket->items[bucket_type::cnBucketLength - nFgItems - 1];
        }        
    }
    return 0;
}

template <typename StorageT, int cnOverflowDepth>
int CChainsTable<StorageT, cnOverflowDepth>::getFirstIndex() const 
{ 
    return m_nFirstBuckIndex; 
}

template <typename StorageT, int cnOverflowDepth>
int CChainsTable<StorageT, cnOverflowDepth>::getLastIndex() const 
{
    return m_nLastBuckIndex; 
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::setFirstIndex(int nIndex) 
{ 
    m_nFirstBuckIndex = nIndex; 
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::setLastIndex(int nIndex) 
{ 
    m_nLastBuckIndex = nIndex; 
    putPotentialFreeBucket(m_nLastBuckIndex, true);
}    

template <typename StorageT, int cnOverflowDepth>
int CChainsTable<StorageT, cnOverflowDepth>::getKeysMaxCount() const
{
    return bucket_type::cnBucketLength * (m_nLastBuckIndex - m_nFirstBuckIndex + 1);
}

template <typename StorageT, int cnOverflowDepth>
int CChainsTable<StorageT, cnOverflowDepth>::getKeysCount() const
{
    return m_nKeysCount;
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::setKeysCount(int nCount)
{
    m_nKeysCount = nCount;
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::putPotentialFreeBucket(int nBuckIndex, bool bForceOut)
{    
    for (int nMapInd = 0; nMapInd < cnOverflowDepth; ++nMapInd)
    {
        const int nIndex = m_arrOverflowMap[nMapInd];

        if (nIndex < m_nFirstBuckIndex || nIndex > m_nLastBuckIndex)
        {
            m_arrOverflowMap[nMapInd] = nBuckIndex;
            return;
        }
    }
    
    if (bForceOut)
    {
        m_arrOverflowMap[0] = nBuckIndex;
    }
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::reset()
{        
    m_nCurrChainIndex = 0;

    m_currChain.m_currBucket = bucket_holder();
    m_currChain.m_overflowBucket = bucket_holder();
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::clear()
{
    reset();

    m_nFirstBuckIndex = 1;
    m_nLastBuckIndex = 1;

    m_nCurrItemIndex = 0;
    m_nKeysCount = 0;

    m_pStorage->clear();
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::swap(CChainsTable<StorageT, cnOverflowDepth>& other)
{   
    std::swap(m_pStorage, other.m_pStorage);
    std::swap(m_indexFunc, other.m_indexFunc);
    std::swap(m_nFirstBuckIndex, other.m_nFirstBuckIndex);
    std::swap(m_nLastBuckIndex, other.m_nLastBuckIndex);
    std::swap(m_nKeysCount, other.m_nKeysCount);
    std::swap(m_nCurrChainIndex, other.m_nCurrChainIndex);
    std::swap(m_nCurrItemIndex, other.m_nCurrItemIndex);
        
    m_currChain.swap(other.m_currChain);
}

template <typename StorageT, int cnOverflowDepth>
typename CChainsTable<StorageT, cnOverflowDepth>::bucket_holder 
    CChainsTable<StorageT, cnOverflowDepth>::getBucket(int nBuckIndex)
{
    assert(nBuckIndex >= m_nFirstBuckIndex && nBuckIndex <= m_nLastBuckIndex);

    const int nPhysIndex = m_indexFunc(nBuckIndex);
    return m_pStorage->readBucket(nPhysIndex);
}

template <typename StorageT, int cnOverflowDepth>
void CChainsTable<StorageT, cnOverflowDepth>::flushBucket(const bucket_holder& bucket, int nBuckIndex)
{
    assert(nBuckIndex >= m_nFirstBuckIndex && nBuckIndex <= m_nLastBuckIndex);

    const int nPhysIndex = m_indexFunc(nBuckIndex);
    m_pStorage->writeBucket(nPhysIndex, bucket);
}

template <typename StorageT, int cnOverflowDepth>
typename CChainsTable<StorageT, cnOverflowDepth>::bucket_holder 
    CChainsTable<StorageT, cnOverflowDepth>::searchOverflowBucket(bool bOverall, int nKeys, unsigned int& nBuckIndex)
{
    for (int nMapInd = 0; nMapInd < cnOverflowDepth; ++nMapInd)
    {
        const int nIndex = m_arrOverflowMap[nMapInd];
        if (nIndex >= m_nFirstBuckIndex && nIndex <= m_nLastBuckIndex)
        {
            bucket_holder bucket = getBucket(nIndex);

            if (0 == bucket->chain.nForeignItems && 
                nKeys <= (bucket_type::cnBucketLength - bucket->chain.nCurrentItem))
            {
                nBuckIndex = nIndex;
                m_arrOverflowMap[nMapInd] = 0;
                return bucket;
            }            
        }
    }

    const int nToScanInd = bOverall ? 
        m_nFirstBuckIndex : std::max(m_nFirstBuckIndex, m_nLastBuckIndex - cnOverflowDepth);
        
    for (int i = m_nLastBuckIndex; i >= nToScanInd; --i)
    {
        bucket_holder bucket = getBucket(i);

        if (0 == bucket->chain.nForeignItems)
        {
            const int nFreeFgItems = (bucket_type::cnBucketLength - bucket->chain.nCurrentItem);
            if (0 != nFreeFgItems)
            {
                putPotentialFreeBucket(i, false);
            }            
            if (nKeys <= nFreeFgItems)
            {
                nBuckIndex = i;
                return bucket;
            }            
        }
    }   
    
    return bucket_holder();
}

template <typename HashKeyT, typename K, int ItemsCount>
class CHashKeys;

template <typename HashKeyT, typename K, int ItemsCount>
struct ChainData
{
    CHashKeys<HashKeyT, K, ItemsCount> keys;

    unsigned char nCurrentItem;
    unsigned char nForeignItems;

    unsigned int nPrevIndex;
    unsigned int nPrimNextIndex;    
    unsigned int nFgNextIndex;
    unsigned int nLastChainIndex;

    ChainData() 
        : nCurrentItem(0), nForeignItems(0), nLastChainIndex(0),
        nPrevIndex(0), nPrimNextIndex(0), nFgNextIndex(0)
    {
    }
};

//hash keys routine for optimization of keys storing
template <typename HashKeyT, typename K, int ItemsCount>
class CHashKeys
{
public:
    void insert(const HashKeyT& hashKey, int nIndex)
    {
        keys[nIndex] = hashKey;
    }    
    void move(int nIndex, const CHashKeys& other, int nOtherIndex)
    {
        keys[nIndex] = other.keys[nOtherIndex];
    }    
    bool compare(const HashKeyT& hashKey, int nIndex) const
    {
        return keys[nIndex] == hashKey;
    }
    template <typename F, typename K> const HashKeyT& obtainHashKey(const F&, const K&, int nIndex) const
    {
        return keys[nIndex];
    }

private:
    HashKeyT keys[ItemsCount];
};

template <typename HashKeyT>
class CRehashObtainBase
{
public:
    void insert(const HashKeyT&, int) {}
    void move(int, const CRehashObtainBase&, int) {}
    bool compare(const HashKeyT&, int) const { return true; }

    template <typename F, typename K> HashKeyT obtainHashKey(const F& hashFun, const K& key, int) const
    {
        return hashFun.process(key);
    }
};

//hash stubs specializations for POD types
struct ValueStub {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, ValueStub, ItemsCount> : public CRehashObtainBase<HashKeyT> {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, int, ItemsCount> : public CRehashObtainBase<HashKeyT> {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, unsigned int, ItemsCount> : public CRehashObtainBase<HashKeyT> {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, long, ItemsCount> : public CRehashObtainBase<HashKeyT> {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, unsigned long, ItemsCount> : public CRehashObtainBase<HashKeyT> {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, short, ItemsCount> : public CRehashObtainBase<HashKeyT> {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, unsigned short, ItemsCount> : public CRehashObtainBase<HashKeyT> {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, char, ItemsCount> : public CRehashObtainBase<HashKeyT> {};
template <typename HashKeyT, int ItemsCount>
class CHashKeys<HashKeyT, unsigned char, ItemsCount> : public CRehashObtainBase<HashKeyT> {};

} //namespace algo

#endif //__CHAINSTABLE_H__