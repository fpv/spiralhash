#ifndef __SPIRALBASE_H__
#define __SPIRALBASE_H__

#include "ChainsTable.h"

namespace algo
{

template <typename TableT, typename KeyT, typename ItemT>
class CItemsTableEnumerator;

template <typename ItemT, typename ConstIteratorT>
class CConstPosition;

template <typename HashFunT, typename StorageT>
class CSpiralBase
{
public:
    // experimentally defined value based on algorithms specifics (free buckets buffer depth)
    static const int cnOverflowDepth = 3; 
    typedef CChainsTable<StorageT, cnOverflowDepth> table_type;
    typedef typename table_type::bucket_type bucket_type;
    typedef typename bucket_type::key_type key_type;
    typedef typename bucket_type::item_type item_type;    
        
    bool empty() const;
    double getLoadFactor() const;    

protected:    
    typedef StorageT storage_type;
    typedef typename table_type::CChain chain_type; 
    typedef typename table_type::ChainBookmark bookmark_type;
    typedef CItemsTableEnumerator<table_type, key_type, item_type> items_enumerator_type;        
    typedef typename HashFunT::value_type hash_key_type;    

    CSpiralBase(StorageT* pStorage);
    ~CSpiralBase();

    void reset(StorageT* pStorage);
        
    chain_type* seekToChain(const hash_key_type& hashKey) const;
    chain_type* findItem(const hash_key_type& hashKey, const key_type& key) const;
    chain_type* findItemFrom(const bookmark_type& bookmark, const hash_key_type& hashKey, const key_type& key) const;
    chain_type* insertItem(const hash_key_type& hashKey, const item_type& item);
    void eraseCurrentItem(); 

    void split();
    void group();
    void clear();
    void swap(CSpiralBase<HashFunT, StorageT>& other);

    size_t getItemsCount() const;
    
    items_enumerator_type getItemsEnumerator() const;    

    double getMinLoadFactor() const;
    double getMaxLoadFactor() const;

private:    
    typedef std::pair<hash_key_type, item_type> buf_pair_type;

    static const double cdMaxLoadFactor;
    static const double cdMinLoadFactor;

    mutable table_type m_table;
    int m_nLevel; 
    int m_nMaxLevelIndex;    
    
    buf_pair_type* m_pSplitBuf;
    size_t m_nSplitBufItem;
    size_t m_nSplitBufPos;
    size_t m_nSplitBufSize;

    int getChainIndex(const hash_key_type& hashKey) const;    
    static int chainIndexToPhysIndex(int nLogicalIndex);
    void growStorage();
    void shrinkStorage();
    void splitHashTable();
    void groupHashTable();    
    void rollbackGrouping();

    void initKeysBuffer();
    void addBufPair(const hash_key_type& hashKey, const item_type& item);
    const buf_pair_type* nextBufPair();
};

template <typename HashFunT, typename StorageT>
const double CSpiralBase<HashFunT, StorageT>::cdMaxLoadFactor = 0.75;

template <typename HashFunT, typename StorageT>
const double CSpiralBase<HashFunT, StorageT>::cdMinLoadFactor = 0.60;

template <typename HashFunT, typename StorageT>
CSpiralBase<HashFunT, StorageT>::CSpiralBase(StorageT* pStorage)
    : m_table(pStorage, &CSpiralBase<HashFunT, StorageT>::chainIndexToPhysIndex), 
    m_nMaxLevelIndex(2 /*maximum buckets count for the next level*/), m_nLevel(0), 
    m_pSplitBuf(0), m_nSplitBufItem(0), m_nSplitBufPos(0), m_nSplitBufSize(0)
{
}

template <typename HashFunT, typename StorageT>
CSpiralBase<HashFunT, StorageT>::~CSpiralBase()
{
    delete [] m_pSplitBuf;
    clear();
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::reset(StorageT* pStorage)
{
    m_table.setStorage(pStorage);
}

template <typename HashFunT, typename StorageT>
typename CSpiralBase<HashFunT, StorageT>::chain_type* 
    CSpiralBase<HashFunT, StorageT>::seekToChain(const hash_key_type& hashKey) const
{
    const int nIndex = getChainIndex(hashKey);
    return m_table.seekToChain(nIndex);
} 

template <typename HashFunT, typename StorageT>
typename CSpiralBase<HashFunT, StorageT>::chain_type* 
    CSpiralBase<HashFunT, StorageT>::findItem(const hash_key_type& hashKey, const key_type& key) const
{        
    chain_type* pChain = seekToChain(hashKey);
    if (pChain->seekItem(hashKey, key))
    {
        return pChain;
    }
    return 0;
}

template <typename HashFunT, typename StorageT>
typename CSpiralBase<HashFunT, StorageT>::chain_type* 
    CSpiralBase<HashFunT, StorageT>::findItemFrom(const bookmark_type& bookmark, const hash_key_type& hashKey, const key_type& key) const
{        
    chain_type* pChain = m_table.seekToChain(bookmark.nChainIndex);
    pChain->setBookmark(bookmark);

    if (pChain->seekItem(hashKey, key))
    {
        return pChain;
    }
    return 0;
}


template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::split()
{
    if (getLoadFactor() > getMaxLoadFactor())
    {
        splitHashTable();        
    }
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::group()
{
    if (m_table.getStorage()->isNeedContract() && 
        m_nLevel > 1 && getLoadFactor() < getMinLoadFactor())
    {
        groupHashTable();
    }    
}

template <typename HashFunT, typename StorageT>
typename CSpiralBase<HashFunT, StorageT>::chain_type* 
    CSpiralBase<HashFunT, StorageT>::insertItem(const hash_key_type& hashKey, const item_type& item)
{
    chain_type* pChain = m_table.getCurrentChain();
    
    int nBuckIndex = pChain->insertItem(hashKey, item, false);
    while(-1 == nBuckIndex)
    {
        splitHashTable();

        pChain = seekToChain(hashKey);
        pChain->seekLastItem();
        nBuckIndex = pChain->insertItem(hashKey, item, false);        
    }

    assert(nBuckIndex != -1);
    return pChain;
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::eraseCurrentItem()
{    
    chain_type* pChain = m_table.getCurrentChain();    
    pChain->eraseItem();
}

template <typename HashFunT, typename StorageT>
typename CSpiralBase<HashFunT, StorageT>::items_enumerator_type 
    CSpiralBase<HashFunT, StorageT>::getItemsEnumerator() const
{
    return items_enumerator_type(m_table, m_table.getFirstIndex());
}

template <typename HashFunT, typename StorageT>
double CSpiralBase<HashFunT, StorageT>::getMinLoadFactor() const
{
    return cdMinLoadFactor;
}

template <typename HashFunT, typename StorageT>
double CSpiralBase<HashFunT, StorageT>::getMaxLoadFactor() const
{
    return cdMaxLoadFactor;
}

template <typename HashFunT, typename StorageT>
int CSpiralBase<HashFunT, StorageT>::getChainIndex(const hash_key_type& hashKey) const
{
    static const double dMaxHashKey = static_cast<double>(std::numeric_limits<hash_key_type>::max()) + 1.0;
    double dSpiralValue = pow(2.0, static_cast<double>(hashKey) / dMaxHashKey);

    int nChainIndex = static_cast<int>(dSpiralValue * (1 << m_nLevel));
    if (nChainIndex < m_table.getFirstIndex())
    {
        nChainIndex = static_cast<int>(dSpiralValue * (1 << (m_nLevel + 1)));
    }

    return nChainIndex;
}

template <typename HashFunT, typename StorageT>
int CSpiralBase<HashFunT, StorageT>::chainIndexToPhysIndex(int nBuckIndex)
{
    assert(0 != nBuckIndex);

    // below algorithms the same as next two rows but more quick
    // while (0 == (nBuckIndex & 0x1)) nBuckIndex >>= 1;        
    // return (nBuckIndex - 1) >> 1;

    static const unsigned char table[256] = 
    {
        8, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        7, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 
        4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0
    };    
    unsigned int n = 24;
    unsigned int x = nBuckIndex;
    unsigned int y = x << 16; 
    if (0 != y) 
    {
        n = n - 16; 
        x = y; 
    }    
    y = x << 8;     
    if (0 != y) 
    { 
        n = n - 8; 
        x = y; 
    }
    n = n + table[x >> 24];
    return ((nBuckIndex >> n) - 1) >> 1;
}


template <typename HashFunT, typename StorageT>
double CSpiralBase<HashFunT, StorageT>::getLoadFactor() const
{
    return static_cast<double>(m_table.getKeysCount()) / m_table.getKeysMaxCount();
}

template <typename HashFunT, typename StorageT>
size_t CSpiralBase<HashFunT, StorageT>::getItemsCount() const
{
    return m_table.getKeysCount();
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::clear()
{        
    m_table.clear();    
    
    m_nLevel = 0;
    m_nMaxLevelIndex = 2; //maximum buckets count for next level    
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::swap(CSpiralBase<HashFunT, StorageT>& other)
{
    m_table.swap(other.m_table);
    std::swap(m_nLevel, other.m_nLevel);
    std::swap(m_nMaxLevelIndex, other.m_nMaxLevelIndex);
}

template <typename HashFunT, typename StorageT>
bool CSpiralBase<HashFunT, StorageT>::empty() const
{
    return 0 == m_table.getKeysCount();
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::growStorage()
{    
    m_table.getStorage()->expand();
    m_table.setFirstIndex(m_table.getFirstIndex() + 1);
    m_table.setLastIndex((m_table.getFirstIndex() << 1) - 1);
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::shrinkStorage()
{
    m_table.getStorage()->contract();
    m_table.setFirstIndex(m_table.getFirstIndex() - 1);
    m_table.setLastIndex((m_table.getFirstIndex() << 1) - 1);
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::splitHashTable()
{
    initKeysBuffer();

    //remove all key from first bucket
    chain_type* pFirstChain = m_table.seekToChain(m_table.getFirstIndex());
    hash_key_type hashKey = 0;    
    HashFunT hashFunc;

    while (const item_type* pChainItem = pFirstChain->detachNextItem(hashFunc, hashKey))
    {
        addBufPair(hashKey, *pChainItem);
    }

    pFirstChain->relinkForeignKeys(m_table.getFirstIndex() << 1);

    //expand table and storage
    growStorage();
    
    while (const buf_pair_type* pBufPair = nextBufPair())
    {
        const item_type& headItem = pBufPair->second;        
        const int nChainIndex = getChainIndex(pBufPair->first);

        chain_type* pChainSplt = m_table.seekToChain(nChainIndex);
        pChainSplt->seekLastItem();
        const int nBuckIndex = pChainSplt->insertItem(pBufPair->first, headItem, true);
        assert(-1 != nBuckIndex);
    }

    if (m_table.getFirstIndex() == m_nMaxLevelIndex)
    {
        ++m_nLevel;
        m_nMaxLevelIndex = m_table.getFirstIndex() + (1 << m_nLevel);
    }
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::groupHashTable()
{    
    assert(m_nLevel > 1); //nothing to shrink on two or one chunks    
    const int nLastBuckIndex = (m_table.getFirstIndex() << 1) - 1;    

    chain_type* pLastChain = m_table.seekToChain(nLastBuckIndex);

    //if last bucket has foreign keys, move them to other bucket
    if (!pLastChain->moveForeignKeys())
    {
        return;
    } 

    initKeysBuffer();
    hash_key_type hashKey = 0;
    HashFunT hashFunc;
    //remove all keys from last bucket
    while (const item_type* pChainItem = pLastChain->detachNextItem(hashFunc, hashKey))
    {    
        addBufPair(hashKey, *pChainItem);
    }

    //remove all keys from previous bucket
    chain_type* pPrevChain = m_table.seekToChain(nLastBuckIndex - 1);
    while (const item_type* pChainItem = pPrevChain->detachNextItem(hashFunc, hashKey))
    {        
        addBufPair(hashKey, *pChainItem);
    }

    //contract table and storage
    shrinkStorage();        

    //if previous bucket has foreign keys, relink ancestor bucket
    pPrevChain->relinkForeignKeys(m_table.getFirstIndex());    

    //update level
    if (m_table.getFirstIndex() == m_nMaxLevelIndex - (1 << m_nLevel) - 1)
    {
        --m_nLevel;
        m_nMaxLevelIndex = m_table.getFirstIndex() + 1;
    }

    //move all keys to previous bucket (first after shrink)        
    chain_type* pFirstChain = m_table.seekToChain(m_table.getFirstIndex());    
    while (const buf_pair_type* pBufPair = nextBufPair())
    {                
        const int nBuckInserted = pFirstChain->insertItem(pBufPair->first, pBufPair->second, true);

        if (-1 == nBuckInserted)        
        {
            rollbackGrouping();
            break;
        }
    }    
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::initKeysBuffer()
{    
    m_nSplitBufItem = 0;
    m_nSplitBufPos = 0;

    if (0 == m_nSplitBufSize)
    {
        delete [] m_pSplitBuf;

        m_nSplitBufSize = bucket_type::cnBucketLength * 2;
        m_pSplitBuf = new buf_pair_type[m_nSplitBufSize];
    }    
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::addBufPair(const hash_key_type& hashKey, const item_type& item)
{
    if (m_nSplitBufSize == m_nSplitBufItem)
    {
        size_t nNewSize = m_nSplitBufSize + bucket_type::cnBucketLength;
        buf_pair_type* pNewBuf = new buf_pair_type[nNewSize];
        
        for (size_t i = 0; i < m_nSplitBufSize; ++i)
        {
            pNewBuf[i] = m_pSplitBuf[i];
        }
        
        delete [] m_pSplitBuf;
        m_pSplitBuf = pNewBuf;
        m_nSplitBufSize = nNewSize;
    }
    
    m_pSplitBuf[m_nSplitBufItem] = buf_pair_type(hashKey, item);
    ++m_nSplitBufItem;
}

template <typename HashFunT, typename StorageT>
const typename CSpiralBase<HashFunT, StorageT>::buf_pair_type* 
    CSpiralBase<HashFunT, StorageT>::nextBufPair()
{
    const buf_pair_type* pBufPair = 0;
    if (m_nSplitBufPos != m_nSplitBufItem)
    {
        pBufPair = &m_pSplitBuf[m_nSplitBufPos];
        ++m_nSplitBufPos;
    }
    return pBufPair;
}

template <typename HashFunT, typename StorageT>
void CSpiralBase<HashFunT, StorageT>::rollbackGrouping()
{   
    struct buf_guard 
    { 
        buf_pair_type* buf; 
        buf_guard(buf_pair_type* b) : buf(b) {} 
        ~buf_guard() { delete [] buf; }
    };

    --m_nSplitBufPos;

    size_t nRollbackSize = m_nSplitBufItem - m_nSplitBufPos;
    buf_pair_type* pRollbackBuf = new buf_pair_type[nRollbackSize];
    buf_guard guard(pRollbackBuf);
        
    size_t nPair = 0;
    const buf_pair_type* pBufPair = 0;
        
    while (pBufPair = nextBufPair())
    {
        pRollbackBuf[nPair] = *pBufPair;
        ++nPair;
    }

    for (nPair = 0; nPair < nRollbackSize; ++nPair)
    {
        pBufPair = &pRollbackBuf[nPair];
        insertItem(pBufPair->first, pBufPair->second);
    }
}

template <typename ItemT, typename ConstIteratorT>
class CConstPosition
{    
    typedef ItemT item_type;
    typedef ConstIteratorT const_iterator;
public:    
    typedef CConstPosition<item_type, const_iterator> const_position;
    typedef const item_type* const_pointer;
    typedef const item_type& const_reference;

    CConstPosition() : m_pItem(0) {}
    CConstPosition(const item_type* pItem) : m_pItem(pItem) {}

    const_reference operator*() const
    {
        return *m_pItem;
    }
    const_pointer operator->() const
    {
        return m_pItem;
    }
    bool operator == (const const_iterator& other)
    {
        return m_pItem == other.m_pItem;
    }
    bool operator != (const const_iterator& other)
    {
        return !(*this == other);
    }

private:
    const item_type* m_pItem;
};

template <typename ItemT, typename KeyT, typename ConstIteratorT>
class CSingleConstPosition
{    
    typedef ItemT item_type;
    typedef KeyT key_type;
    typedef ConstIteratorT const_iterator;
public:    
    typedef CSingleConstPosition<item_type, key_type, const_iterator> const_position;
    typedef const key_type* const_pointer;
    typedef const key_type& const_reference;    

    CSingleConstPosition() : m_pItem(0) {}
    CSingleConstPosition(const item_type* pItem) : m_pItem(pItem) {}

    const_reference operator*() const
    {
        return m_pItem->first;
    }
    const_pointer operator->() const
    {
        return &m_pItem->first;
    }
    bool operator == (const const_iterator& other)
    {
        return m_pItem == other.m_pItem;
    }
    bool operator != (const const_iterator& other)
    {
        return !(*this == other);
    }

private:
    const item_type* m_pItem;
};


template <typename TableT, typename KeyT, typename ItemT>
class CItemsTableEnumerator
{    
public:
    typedef TableT table_type;
    typedef KeyT key_type;
    typedef ItemT item_type;

    CItemsTableEnumerator();
    CItemsTableEnumerator(const table_type& table, int nFirstIndex);
    CItemsTableEnumerator(const CItemsTableEnumerator& other);
    CItemsTableEnumerator& operator = (const CItemsTableEnumerator& other);

    const item_type* currItem() const;
    const item_type* nextItem();
    bool empty() const;

private:
    table_type m_table;
    int m_nCurrChainIndex;  
};

template <typename TableT, typename KeyT, typename ItemT>
CItemsTableEnumerator<TableT, KeyT, ItemT>::CItemsTableEnumerator()
    : m_table(0, 0), m_nCurrChainIndex(0)
{
}

template <typename TableT, typename KeyT, typename ItemT>
CItemsTableEnumerator<TableT, KeyT, ItemT>::CItemsTableEnumerator(const table_type& table, int nFirstIndex)
    : m_table(table), m_nCurrChainIndex(nFirstIndex)
{
    m_table.seekToChain(nFirstIndex);    
}

template <typename TableT, typename KeyT, typename ItemT>
CItemsTableEnumerator<TableT, KeyT, ItemT>::CItemsTableEnumerator(const CItemsTableEnumerator& other)
    : m_table(other.m_table), m_nCurrChainIndex(other.m_nCurrChainIndex)    
{
}

template <typename TableT, typename KeyT, typename ItemT>
CItemsTableEnumerator<TableT, KeyT, ItemT>& 
    CItemsTableEnumerator<TableT, KeyT, ItemT>::operator = (const CItemsTableEnumerator& other)
{
    m_table = other.m_table;
    m_nCurrChainIndex = other.m_nCurrChainIndex;    
    return *this;
}

template <typename TableT, typename KeyT, typename ItemT>
const typename CItemsTableEnumerator<TableT, KeyT, ItemT>::item_type* 
    CItemsTableEnumerator<TableT, KeyT, ItemT>::currItem() const
{
    return m_table.currBucketItem();
}

template <typename TableT, typename KeyT, typename ItemT>
const typename CItemsTableEnumerator<TableT, KeyT, ItemT>::item_type* 
    CItemsTableEnumerator<TableT, KeyT, ItemT>::nextItem()
{
    const item_type* pItem = m_table.nextBucketItem();
    
    while (0 == pItem)
    {
        if (m_nCurrChainIndex == m_table.getLastIndex())
        {
            m_table.reset();
            return 0;
        }
        
        m_table.seekToChain(++m_nCurrChainIndex);
        pItem = m_table.currBucketItem();            
    }

    return pItem;
}

template <typename TableT, typename KeyT, typename ItemT>
bool CItemsTableEnumerator<TableT, KeyT, ItemT>::empty() const
{    
    return (0 == m_nCurrChainIndex);
}

} //namespace algo

#endif //__SPIRALBASE_H__