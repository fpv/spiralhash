#ifndef __SPIRALSET_H__
#define __SPIRALSET_H__

#include "ChainsTable.h"
#include "SpiralBase.h"

namespace algo
{

template <typename HashKeyT, typename K, int BucketLength = 32>
struct SetBucket
{        
    struct single
    {         
        K first;
        
        single() {}
        single(const K& f) : first(f) {}
    };
    
    typedef HashKeyT hash_key_type;
    typedef K key_type;    
    typedef single item_type;    

    static const int cnBucketLength = BucketLength;        
    ChainData<HashKeyT, K, BucketLength> chain;
    item_type items[BucketLength];    
};

template <typename EnumeratorT>
class CSpiralSetConstIterator;

template <typename HashFunT, typename StorageT>
class CSpiralSetImpl : public CSpiralBase<HashFunT, StorageT>
{      
    typedef CSpiralSetImpl<HashFunT, StorageT> this_type;
    typedef CSpiralBase<HashFunT, StorageT> spiral_base;    
public:            
    typedef CSpiralSetConstIterator<items_enumerator_type> const_iterator;
    typedef CSingleConstPosition<item_type, key_type, const_iterator> const_position;    

    CSpiralSetImpl(StorageT* pStorage);

    std::pair<const_position, bool> insert(const key_type& key);
    const_position find(const key_type& key) const;
    size_t erase(const key_type& key);
    void clear();
    void swap(this_type& other);

    size_t size() const;

    const_iterator begin() const;
    const_iterator end() const;    
};

template <typename HashFunT, typename StorageT>
CSpiralSetImpl<HashFunT, StorageT>::CSpiralSetImpl(StorageT* pStorage)
    : spiral_base(pStorage)
{
}

template <typename HashFunT, typename StorageT>
std::pair<typename CSpiralSetImpl<HashFunT, StorageT>::const_position, bool> 
    CSpiralSetImpl<HashFunT, StorageT>::insert(const key_type& key)
{        
    spiral_base::split();

    const hash_key_type hashKey = HashFunT().process(key);        
    chain_type* pChain = 0;
    
    //if key already added to chunk, then exit    
    if (0 != (pChain = spiral_base::findItem(hashKey, key)))
    {
        return std::pair<const_position, bool>(const_position(pChain->currItem()), false);
    }
    
    //insert new key
    const bucket_type::single item(key);
    pChain = spiral_base::insertItem(hashKey, item);

    return std::pair<const_position, bool>(const_position(pChain->currItem()), true);    
}

template <typename HashFunT, typename StorageT>
typename CSpiralSetImpl<HashFunT, StorageT>::const_position 
    CSpiralSetImpl<HashFunT, StorageT>::find(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);
    if (0 != pChain)
    {
        return const_position(pChain->currItem());
    }    
    return const_position();
}

template <typename HashFunT, typename StorageT>
size_t CSpiralSetImpl<HashFunT, StorageT>::erase(const key_type& key)
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);
    if (0 != pChain)
    {
        spiral_base::eraseCurrentItem();
        spiral_base::group();

        return 1;
    }
    return 0;
}

template <typename HashFunT, typename StorageT>
void CSpiralSetImpl<HashFunT, StorageT>::clear()
{
    spiral_base::clear();
}

template <typename HashFunT, typename StorageT>
void CSpiralSetImpl<HashFunT, StorageT>::swap(this_type& other)
{
    spiral_base::swap(other);
}

template <typename HashFunT, typename StorageT>
size_t CSpiralSetImpl<HashFunT, StorageT>::size() const
{
    return spiral_base::getItemsCount();
}

template <typename HashFunT, typename StorageT>
typename CSpiralSetImpl<HashFunT, StorageT>::const_iterator 
    CSpiralSetImpl<HashFunT, StorageT>::begin() const
{
    return const_iterator(getItemsEnumerator());
}

template <typename HashFunT, typename StorageT>
typename CSpiralSetImpl<HashFunT, StorageT>::const_iterator 
    CSpiralSetImpl<HashFunT, StorageT>::end() const
{
    return const_iterator();
}

template <typename EnumeratorT>
class CSpiralSetConstIterator
{    
    typedef EnumeratorT enumerator_type;
    typedef typename enumerator_type::item_type item_type;
    typedef typename enumerator_type::key_type key_type;

    friend CSingleConstPosition<item_type, key_type, CSpiralSetConstIterator<enumerator_type> >;    

public:    
    typedef CSpiralSetConstIterator<enumerator_type> const_iterator;
    typedef const key_type* const_pointer;
    typedef const key_type& const_reference;

    CSpiralSetConstIterator()
        : m_pItem(0)
    {
    }

    CSpiralSetConstIterator(const enumerator_type& enumerator)
        : m_pItem(0), m_enumerator(enumerator)
    {         
        m_pItem = m_enumerator.currItem();
    }

    CSpiralSetConstIterator(const CSpiralSetConstIterator& other)
        : m_pItem(0), m_enumerator(other.m_enumerator)
    {
        m_pItem = m_enumerator.currItem();
    }

    CSpiralSetConstIterator& operator = (const CSpiralSetConstIterator& other)
    {        
        m_enumerator = other.m_enumerator;
        m_pItem = m_enumerator.currItem();
        return *this;
    }

    const_reference operator*() const
    {
        return m_pItem->first;
    }

    const_pointer operator->() const
    {
        return &m_pItem->first;
    }

    const_iterator& operator++()
    {
        m_pItem = m_enumerator.nextItem();
        return *this;
    }

    const_iterator operator++(int)
    {        
        CSpiralSetConstIterator prevIter = *this; 
        m_pItem = m_enumerator.nextItem();
        return prevIter;
    }

    bool operator == (const const_iterator& other)
    {
        return (m_pItem == other.m_pItem);
    }

    bool operator != (const const_iterator& other)
    {
        return !(*this == other);
    }

private:
    const item_type* m_pItem;
    enumerator_type m_enumerator;
};

} //namespace algo

#endif //__SPIRALSET_H__
