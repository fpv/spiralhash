#ifndef __SPIRALSPARSEMULTIMAP_H__
#define __SPIRALSPARSEMULTIMAP_H__

#include <utility>
#include "SpiralSparseMultiset.h"
#include "SpiralMap.h"

namespace algo
{

template <typename HashFunT, typename StorageT>
class CSpiralSparseMultimapImpl : public CSpiralBase<HashFunT, StorageT>
{
    typedef CSpiralSparseMultimapImpl<HashFunT, StorageT> this_type;
    typedef CSpiralBase<HashFunT, StorageT> spiral_base;
    typedef CConstPosition<item_type, CSpiralMapConstIterator<items_enumerator_type> > base_position_type;    
public:
    typedef CSpiralMapConstIterator<items_enumerator_type> const_iterator;
    typedef CSpiralMultiPosition<table_type, base_position_type> const_position;

    CSpiralSparseMultimapImpl(StorageT* pStorage);

    void insert(const item_type& item);
    const_position find(const key_type& key) const;
    const_position find(const const_position& prevPos) const;
    bool contains(const key_type& key) const;
    size_t erase(const key_type& key);
    void clear();
    void swap(this_type& other);
    size_t size() const;    

    const_iterator begin() const;
    const_iterator end() const;
};

template <typename HashFunT, typename StorageT>
CSpiralSparseMultimapImpl<HashFunT, StorageT>::CSpiralSparseMultimapImpl(StorageT* pStorage)
    : spiral_base(pStorage)
{
}

template <typename HashFunT, typename StorageT>
void CSpiralSparseMultimapImpl<HashFunT, StorageT>::insert(const item_type& item)
{
    spiral_base::split();

    const hash_key_type hashKey = HashFunT().process(item.first);
    chain_type* pChain = seekToChain(hashKey);
    pChain->seekLastItem();
   
    spiral_base::insertItem(hashKey, item);    
}

template <typename HashFunT, typename StorageT>
typename CSpiralSparseMultimapImpl<HashFunT, StorageT>::const_position 
    CSpiralSparseMultimapImpl<HashFunT, StorageT>::find(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);
    if (0 != pChain)
    {        
        return const_position(pChain);
    }
    return const_position();    
}

template <typename HashFunT, typename StorageT>
typename CSpiralSparseMultimapImpl<HashFunT, StorageT>::const_position 
    CSpiralSparseMultimapImpl<HashFunT, StorageT>::find(const const_position& prevPos) const
{
    const bookmark_type& bookmark = prevPos.getBookmark();    
    const hash_key_type hashKey = HashFunT().process(prevPos->first);
    chain_type* pChain = spiral_base::findItemFrom(bookmark, hashKey, prevPos->first);    
    if (0 != pChain)
    {        
        return const_position(pChain);
    }
    return const_position();    
}

template <typename HashFunT, typename StorageT>
bool CSpiralSparseMultimapImpl<HashFunT, StorageT>::contains(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    return (0 != spiral_base::findItem(hashKey, key));
}

template <typename HashFunT, typename StorageT>
size_t CSpiralSparseMultimapImpl<HashFunT, StorageT>::erase(const key_type& key)
{
    const hash_key_type hashKey = HashFunT().process(key);
    size_t nRepeats = 0;
    while (0 != spiral_base::findItem(hashKey, key))
    {
        spiral_base::eraseCurrentItem();
        spiral_base::group();
        ++nRepeats;
    }
    return nRepeats;
}

template <typename HashFunT, typename StorageT>
void CSpiralSparseMultimapImpl<HashFunT, StorageT>::clear()
{
    spiral_base::clear();
}

template <typename HashFunT, typename StorageT>
void CSpiralSparseMultimapImpl<HashFunT, StorageT>::swap(this_type& other)
{
    spiral_base::swap(other);
}

template <typename HashFunT, typename StorageT>
size_t CSpiralSparseMultimapImpl<HashFunT, StorageT>::size() const
{
    return spiral_base::getItemsCount();
}

template <typename HashFunT, typename StorageT>
typename CSpiralSparseMultimapImpl<HashFunT, StorageT>::const_iterator 
    CSpiralSparseMultimapImpl<HashFunT, StorageT>::begin() const
{
    return const_iterator(getItemsEnumerator());
}

template <typename HashFunT, typename StorageT>
typename CSpiralSparseMultimapImpl<HashFunT, StorageT>::const_iterator 
    CSpiralSparseMultimapImpl<HashFunT, StorageT>::end() const
{
    return const_iterator();
}

} //namespace algo

#endif //__SPIRALSPARSEMULTIMAP_H__
