#ifndef __HEAPSTORAGE_H__
#define __HEAPSTORAGE_H__

#include <vector>

namespace algo
{

template <typename BucketT, typename BucketAllocatorT = std::allocator<BucketT> >
class CHeapStorage
{
public:
    typedef typename BucketT::item_type item_type;
    typedef typename BucketT bucket_type;        
    typedef BucketAllocatorT bucket_allocator_type;    

    class CBucketHolder
    {    
    public:
        CBucketHolder() : m_pBucket(0) {}
        CBucketHolder(BucketT* pBucket) : m_pBucket(pBucket) {}
        CBucketHolder(const CBucketHolder& other) : m_pBucket(other.m_pBucket) {}
        CBucketHolder& operator = (const CBucketHolder& other)
        {
            m_pBucket = other.m_pBucket;            
            return *this;
        }
        BucketT* get() { return m_pBucket; }
        const BucketT* get() const { return m_pBucket; } 
        BucketT* operator -> () { return get(); }
        const BucketT* operator -> () const { return get(); }
        void swap(CBucketHolder& other) { std::swap(m_pBucket, other.m_pBucket); }
    private:        
        BucketT* m_pBucket;        
    };

    CHeapStorage();
    CHeapStorage(const bucket_allocator_type& allocator);

    CBucketHolder readBucket(int nPhysBuckIndex);
    void writeBucket(int, const CBucketHolder&) {}

    void expand();    
    void clear();

    void contract() {}
    void shrink() {}    
    bool isNeedContract() const { return false; }    

    void swap(CHeapStorage& other);

private:
    std::vector<BucketT> m_vecBuckets;
};

template <typename BucketT, typename BucketAllocatorT>
CHeapStorage<BucketT, BucketAllocatorT>::CHeapStorage()
    : m_vecBuckets(BucketAllocatorT())
{
}

template <typename BucketT, typename BucketAllocatorT>
CHeapStorage<BucketT, BucketAllocatorT>::CHeapStorage(const bucket_allocator_type& allocator)
    : m_vecBuckets(allocator)
{
}

template <typename BucketT, typename BucketAllocatorT>
typename CHeapStorage<BucketT, BucketAllocatorT>::CBucketHolder 
    CHeapStorage<BucketT, BucketAllocatorT>::readBucket(int nPhysBuckIndex)
{    
    if (m_vecBuckets.empty())
    {
        expand();
    }
    return CBucketHolder(&m_vecBuckets[nPhysBuckIndex]);
}

template <typename BucketT, typename BucketAllocatorT>
void CHeapStorage<BucketT, BucketAllocatorT>::expand()
{
    m_vecBuckets.resize(m_vecBuckets.size() + 1);
}

template <typename BucketT, typename BucketAllocatorT>
void CHeapStorage<BucketT, BucketAllocatorT>::clear()
{
    m_vecBuckets.swap( std::vector<BucketT>() );
}

template <typename BucketT, typename BucketAllocatorT>
void CHeapStorage<BucketT, BucketAllocatorT>::swap(CHeapStorage& other)
{
    m_vecBuckets.swap(other.m_vecBuckets);
}

} //namespace algo

#endif //__HEAPSTORAGE_H__

