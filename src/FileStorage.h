#ifndef __FILESTORAGE_H__
#define __FILESTORAGE_H__

#include <io.h>
#include <stdlib.h>
#include <assert.h>
#include <exception>

namespace algo
{

template <typename BucketT, typename BucketAllocatorT = std::allocator<BucketT> >
class CFileStorage
{
    typedef __int64 file_size_t;

public:    
    typedef typename BucketT::item_type item_type;
    typedef typename BucketT bucket_type;
    typedef BucketAllocatorT bucket_allocator_type;    

    class CBucketHolder
    {    
    public:
        CBucketHolder() : m_pBucket(0), m_pAllocator(0) {}
        CBucketHolder(BucketT* pBucket, bucket_allocator_type* pAllocator) 
            : m_pBucket(pBucket), m_pAllocator(pAllocator) {}
        CBucketHolder(const CBucketHolder& other) 
            : m_pBucket(0), m_pAllocator(0) { *this = other; }
        CBucketHolder& operator = (const CBucketHolder& other)
        {
            if (m_pBucket != other.m_pBucket)
            {         
                destroy();

                m_pBucket = other.m_pBucket;
                m_pAllocator = other.m_pAllocator;
                other.m_pBucket = 0;
            }            
            return *this;
        }
        ~CBucketHolder() 
        {
            destroy();
        }
        BucketT* get() { return m_pBucket; }
        const BucketT* get() const { return m_pBucket; } 
        BucketT* operator -> () { return get(); }
        const BucketT* operator -> () const { return get(); }
        void swap(CBucketHolder& other) { std::swap(m_pBucket, other.m_pBucket); }
    private:        
        mutable BucketT* m_pBucket;
        bucket_allocator_type* m_pAllocator;

        void destroy()
        {
            if (0 != m_pBucket)
            {
                m_pAllocator->destroy(m_pBucket);
                m_pAllocator->deallocate(m_pBucket, 1);
            }
        }
    };

    CFileStorage(FILE* file, const bucket_allocator_type& allocator);
    CFileStorage(FILE* file);

    CBucketHolder readBucket(int nPhysBuckIndex);
    void writeBucket(int nPhysBuckIndex, const CBucketHolder& bucket);

    void expand() {}
    void contract();
    void clear();
    
    void shrink();   
    bool isNeedContract() const { return true; }

    void swap(CFileStorage& other);

private:
    FILE* m_file;
    int m_fd;
    file_size_t m_size;    

    bucket_allocator_type m_allocator;

    void initialize(FILE* file);
    file_size_t getOffset(int nPhysBuckIndex) const
    {        
        return static_cast<file_size_t>(nPhysBuckIndex) * sizeof(BucketT);   
    }    
};

template <typename BucketT, typename BucketAllocatorT>
CFileStorage<BucketT, BucketAllocatorT>::CFileStorage(FILE* file, const bucket_allocator_type& allocator)
    : m_file(file), m_fd(0), m_size(0), m_allocator(allocator)
{
    initialize(m_file);
}

template <typename BucketT, typename BucketAllocatorT>
CFileStorage<BucketT, BucketAllocatorT>::CFileStorage(FILE* file)
    : m_file(file), m_fd(0), m_size(0)
{   
    initialize(m_file);
}

template <typename BucketT, typename BucketAllocatorT>
void CFileStorage<BucketT, BucketAllocatorT>::initialize(FILE* file)
{
    assert(0 != m_file);

    if (0 != _fseeki64(m_file, 0, SEEK_END))
        throw std::exception("file storage initialization error");

    m_size = _ftelli64(m_file);
    m_fd = _fileno(m_file);
}

template <typename BucketT, typename BucketAllocatorT>
typename CFileStorage<BucketT, BucketAllocatorT>::CBucketHolder 
    CFileStorage<BucketT, BucketAllocatorT>::readBucket(int nPhysBuckIndex)
{    
    BucketT* pBukcet = m_allocator.allocate(1);
    m_allocator.construct(pBukcet, BucketT());
    CBucketHolder bucket(pBukcet, &m_allocator);

    file_size_t nFileOffset = getOffset(nPhysBuckIndex);       

    if (nFileOffset + sizeof(BucketT) <= m_size)
    {
        if (0 != _fseeki64(m_file, nFileOffset, SEEK_SET))
            throw std::exception("file storage seek error");

        if (1 != fread(reinterpret_cast<void*>(bucket.get()), sizeof(BucketT), 1, m_file))
            throw std::exception("file storage read error");
    }

    return bucket;
}

template <typename BucketT, typename BucketAllocatorT>
void CFileStorage<BucketT, BucketAllocatorT>::writeBucket( int nPhysBuckIndex, const CBucketHolder& bucket)
{
    const file_size_t nFileOffset = getOffset(nPhysBuckIndex);

    if (nFileOffset > m_size)
    {        
        _chsize_s(m_fd, nFileOffset);
        m_size = nFileOffset;
    }

    if (0 != _fseeki64(m_file, nFileOffset, SEEK_SET))
        throw std::exception("file storage seek error");

    if (1 != fwrite(reinterpret_cast<const void*>(bucket.get()), sizeof(BucketT), 1, m_file))
        throw std::exception("file storage write error");

    if (nFileOffset == m_size)
        m_size += sizeof(BucketT);
}

template <typename BucketT, typename BucketAllocatorT>
void CFileStorage<BucketT, BucketAllocatorT>::contract()
{
    m_size -= sizeof(BucketT);
}

template <typename BucketT, typename BucketAllocatorT>
void CFileStorage<BucketT, BucketAllocatorT>::clear()
{
    m_size = 0;
}

template <typename BucketT, typename BucketAllocatorT>
void CFileStorage<BucketT, BucketAllocatorT>::shrink()
{    
    _chsize_s(m_fd, m_size);
}

template <typename BucketT, typename BucketAllocatorT>
void CFileStorage<BucketT, BucketAllocatorT>::swap(CFileStorage& other)
{
    std::swap(m_file, other.m_file);
    std::swap(m_fd, other.m_fd);
    std::swap(m_size, other.m_size);
    std::swap(m_allocator, other.m_allocator);
}

} //namespace algo

#endif //__FILESTORAGE_H__
