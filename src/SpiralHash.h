#ifndef __SPIRALHASH_H__
#define __SPIRALHASH_H__

#include "DefHashFunc.h"
#include "PagedStorage.h"
#include "HeapStorage.h"
#include "SpiralSet.h"
#include "SpiralDenseMultiset.h"
#include "SpiralSparseMultimap.h"
#include "SpiralMap.h"
#include "SpiralDenseMultimap.h"
#include "SpiralSparseMultimap.h"

namespace algo
{

template <typename BaseImplT>
class CSpiralBaseImplT : public BaseImplT
{
    typedef typename BaseImplT::storage_type storage_type;
    typedef typename BaseImplT base_type;
public:
    CSpiralBaseImplT() : base_type(0)
    {
        reset(&m_storage);
    }
    void swap(CSpiralBaseImplT& other)
    {
        base_type::swap(other);
        m_storage.swap(other.m_storage);
    }
private:
    storage_type m_storage;
};

template <typename BaseImplT>
class CSpiralDenseMapBaseImplT : public BaseImplT
{
    typedef typename BaseImplT::storage_type storage_type;
    typedef typename BaseImplT::value_storage_type value_storage_type;
    typedef typename BaseImplT base_type;
public:
    CSpiralDenseMapBaseImplT() : base_type(0, 0)
    {
        reset(&m_keyStorage, &m_valStorage);
    }
    void swap(CSpiralDenseMapBaseImplT& other)
    {
        base_type::swap(other);
        m_keyStorage.swap(other.m_keyStorage);
        m_valStorage.swap(other.m_valStorage);
    }
private:
    storage_type m_keyStorage;
    value_storage_type m_valStorage;
};


//specializations based on paged storage

//set and multiset
template <typename K, typename HashFunT>
class CSetPagedStorage : public CPagedStorage< SetBucket<typename HashFunT::value_type, K> > {};

template <typename K, typename HashFunT>
class CDenseMultisetPagedStorage : public CPagedStorage< DenseMultisetBucket<typename HashFunT::value_type, K> > {};

template <typename K, typename HashFunT = CDefHashFunc<K> >
class CPagedSpiralSet : public CSpiralBaseImplT< CSpiralSetImpl<HashFunT, CSetPagedStorage<K, HashFunT> > > {};

template <typename K, typename HashFunT = CDefHashFunc<K> >
class CPagedSpiralMultiset : public CSpiralBaseImplT< CSpiralDenseMultisetImpl<HashFunT, CDenseMultisetPagedStorage<K, HashFunT> > > {};

template <typename K, typename HashFunT = CDefHashFunc<K> >
class CPagedSpiralSparseMultiset : public CSpiralBaseImplT< CSpiralSparseMultisetImpl<HashFunT, CSetPagedStorage<K, HashFunT> > > {};

//map and multimap
template <typename K, typename V, typename HashFunT>
class CMapPagedStorage : public CPagedStorage< MapBucket<typename HashFunT::value_type, K, V> > {};

template <typename K, typename V, typename HashFunT>
class CDenseMultimapPagedStorage : public CPagedStorage< DenseMultimapBucket<typename HashFunT::value_type, K, V> > {};

template <typename V>
class CValuesPagedStorage : public CPagedStorage< ValuesMultimapBucket<V> > {};

template <typename K, typename V, typename HashFunT = CDefHashFunc<K> >
class CPagedSpiralMap : public CSpiralBaseImplT< CSpiralMapImpl<HashFunT, CMapPagedStorage<K, V, HashFunT> > > {};

template <typename K, typename V, typename HashFunT = CDefHashFunc<K> >
class CPagedSpiralMultimap : public CSpiralDenseMapBaseImplT< CSpiralDenseMultimapImpl<HashFunT, CDenseMultimapPagedStorage<K, V, HashFunT>, CValuesPagedStorage<V> > > {};

template <typename K, typename V, typename HashFunT = CDefHashFunc<K> >
class CPagedSpiralSparseMultimap : public CSpiralBaseImplT< CSpiralSparseMultimapImpl<HashFunT, CMapPagedStorage<K, V, HashFunT> > > {};

//heap based storage

//set and multiset
template <typename K, typename HashFunT>
class CSetHeapStorage : public CHeapStorage< SetBucket<typename HashFunT::value_type, K> > {};

template <typename K, typename HashFunT>
class CDenseMultisetHeapStorage : public CHeapStorage< DenseMultisetBucket<typename HashFunT::value_type, K> > {};

template <typename K, typename HashFunT = CDefHashFunc<K> >
class CSpiralSet : public CSpiralBaseImplT< CSpiralSetImpl<HashFunT, CSetHeapStorage<K, HashFunT> > > {};

template <typename K, typename HashFunT = CDefHashFunc<K> >
class CSpiralMultiset : public CSpiralBaseImplT< CSpiralDenseMultisetImpl<HashFunT, CDenseMultisetHeapStorage<K, HashFunT> > > {};

template <typename K, typename HashFunT = CDefHashFunc<K> >
class CSpiralSparseMultiset : public CSpiralBaseImplT< CSpiralSparseMultisetImpl<HashFunT, CSetHeapStorage<K, HashFunT> > > {};

//map and multimap
template <typename K, typename V, typename HashFunT>
class CMapHeapStorage : public CHeapStorage< MapBucket<typename HashFunT::value_type, K, V> > {};

template <typename K, typename V, typename HashFunT>
class CDenseMultimapHeapStorage : public CHeapStorage< DenseMultimapBucket<typename HashFunT::value_type, K, V> > {};

template <typename V>
class CValuesHeapStorage : public CHeapStorage< ValuesMultimapBucket<V> > {};

template <typename K, typename V, typename HashFunT = CDefHashFunc<K> >
class CSpiralMap : public CSpiralBaseImplT< CSpiralMapImpl<HashFunT, CMapHeapStorage<K, V, HashFunT> > > {};

template <typename K, typename V, typename HashFunT = CDefHashFunc<K> >
class CSpiralMultimap : public CSpiralDenseMapBaseImplT< CSpiralDenseMultimapImpl<HashFunT, CDenseMultimapHeapStorage<K, V, HashFunT>, CValuesHeapStorage<V> > > {};

template <typename K, typename V, typename HashFunT = CDefHashFunc<K> >
class CSpiralSparseMultimap : public CSpiralBaseImplT< CSpiralSparseMultimapImpl<HashFunT, CMapHeapStorage<K, V, HashFunT> > > {};

} //namespace algo

#endif //__SPIRALHASH_H__