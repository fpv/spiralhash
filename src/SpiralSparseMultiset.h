#ifndef __SPIRALSPARSEMULTISET_H__
#define __SPIRALSPARSEMULTISET_H__

#include "ChainsTable.h"
#include "SpiralBase.h"
#include "SpiralSet.h"

namespace algo
{

template <typename TableT, typename BasePositionT>
class CSpiralMultiPosition;

template <typename HashFunT, typename StorageT>
class CSpiralSparseMultisetImpl : public CSpiralBase<HashFunT, StorageT>
{
    typedef CSpiralSparseMultisetImpl<HashFunT, StorageT> this_type;
    typedef CSpiralBase<HashFunT, StorageT> spiral_base;    
    typedef CSingleConstPosition<item_type, key_type, CSpiralSetConstIterator<items_enumerator_type> > base_position_type;    
public:
    typedef CSpiralSetConstIterator<items_enumerator_type> const_iterator;    
    typedef CSpiralMultiPosition<table_type, base_position_type> const_position;    

    CSpiralSparseMultisetImpl(StorageT* pStorage);

    void insert(const key_type& key);
    const_position find(const key_type& key) const;
    const_position find(const const_position& prevPos) const;
    bool contains(const key_type& key) const;
    size_t erase(const key_type& key);
    void clear();
    void swap(this_type& other);
    size_t size() const;

    const_iterator begin() const;
    const_iterator end() const;
};

template <typename HashFunT, typename StorageT>
CSpiralSparseMultisetImpl<HashFunT, StorageT>::CSpiralSparseMultisetImpl(StorageT* pStorage)
    : spiral_base(pStorage)
{
}

template <typename HashFunT, typename StorageT>
void CSpiralSparseMultisetImpl<HashFunT, StorageT>::insert(const key_type& key)
{
    spiral_base::split();
    
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = seekToChain(hashKey);
    pChain->seekLastItem();

    const bucket_type::single item(key);
    spiral_base::insertItem(hashKey, item);    
}

template <typename HashFunT, typename StorageT>
typename CSpiralSparseMultisetImpl<HashFunT, StorageT>::const_position 
    CSpiralSparseMultisetImpl<HashFunT, StorageT>::find(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    chain_type* pChain = spiral_base::findItem(hashKey, key);    
    if (0 != pChain)
    {        
        return const_position(pChain);
    }
    return const_position();    
}

template <typename HashFunT, typename StorageT>
typename CSpiralSparseMultisetImpl<HashFunT, StorageT>::const_position 
    CSpiralSparseMultisetImpl<HashFunT, StorageT>::find(const const_position& prevPos) const
{
    const bookmark_type& bookmark = prevPos.getBookmark();    
    const hash_key_type hashKey = HashFunT().process(*prevPos);
    chain_type* pChain = spiral_base::findItemFrom(bookmark, hashKey, *prevPos);    
    if (0 != pChain)
    {        
        return const_position(pChain);
    }
    return const_position();    
}

template <typename HashFunT, typename StorageT>
bool CSpiralSparseMultisetImpl<HashFunT, StorageT>::contains(const key_type& key) const
{
    const hash_key_type hashKey = HashFunT().process(key);
    return (0 != spiral_base::findItem(hashKey, key));
}

template <typename HashFunT, typename StorageT>
size_t CSpiralSparseMultisetImpl<HashFunT, StorageT>::erase(const key_type& key)
{
    const hash_key_type hashKey = HashFunT().process(key);
    size_t nRepeats = 0;
    
    while (0 != spiral_base::findItem(hashKey, key))
    {
        spiral_base::eraseCurrentItem();
        spiral_base::group();
        ++nRepeats;
    }
    return nRepeats;
}

template <typename HashFunT, typename StorageT>
void CSpiralSparseMultisetImpl<HashFunT, StorageT>::clear()
{
    spiral_base::clear();
}

template <typename HashFunT, typename StorageT>
void CSpiralSparseMultisetImpl<HashFunT, StorageT>::swap(this_type& other)
{
    spiral_base::swap(other);
}

template <typename HashFunT, typename StorageT>
size_t CSpiralSparseMultisetImpl<HashFunT, StorageT>::size() const
{
    return spiral_base::getItemsCount();
}

template <typename HashFunT, typename StorageT>
typename CSpiralSparseMultisetImpl<HashFunT, StorageT>::const_iterator 
    CSpiralSparseMultisetImpl<HashFunT, StorageT>::begin() const
{
    return const_iterator(getItemsEnumerator());
}

template <typename HashFunT, typename StorageT>
typename CSpiralSparseMultisetImpl<HashFunT, StorageT>::const_iterator 
    CSpiralSparseMultisetImpl<HashFunT, StorageT>::end() const
{
    return const_iterator();
}

template <typename TableT, typename BasePositionT>
class CSpiralMultiPosition : public BasePositionT
{
    typedef BasePositionT base_type;
    typedef TableT table_type;
    typedef typename table_type::CChain chain_type;
    typedef typename table_type::ChainBookmark bookmark_type;
public:
    CSpiralMultiPosition() {}
    CSpiralMultiPosition(const chain_type* pChain) 
        : base_type(pChain->currItem()), m_bookmark(pChain->getBookmark())
    {        
    }

    const bookmark_type& getBookmark() const
    {
        return m_bookmark;
    }

private:
    bookmark_type m_bookmark;
};

} //namespace algo

#endif //__SPIRALSPARSEMULTISET_H__